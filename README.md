# Air Quality Processor

Solution is built upon DEBS Grand Challenge 2021 requirements, available at: `https://2021.debs.org/call-for-grand-challenge-solutions/`. 
Its' main idea is to detect areas in which the air quality index (AQI) improved the most when compared to the previous year. 

Solution is completely based on Apache Flink and can be run on any cluster being able to run Apache Flink applications. 

Following configurations may be defined:
- --process_parallelism <processing parallelism (default is 4) - not bigger than number of tasks slots>
- --city_finder_parallelism <int (default is process_parallelism)>
- --source_parallelism <int (default is process_parallelism)>
- --time_to_live <long (default is 60000ms (one minute))>
- --max_batches <long (default is Long.MAX_VALUE)>
- --use_spatial_index <boolean (default is true)>
- --source_sleep_ms <int (default is 0)>
- --execution_type <string evaluation/test (default is test)>
