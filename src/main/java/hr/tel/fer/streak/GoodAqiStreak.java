package hr.tel.fer.streak;

import java.util.Objects;

public class GoodAqiStreak {
    public int bucketFrom;
    public int bucketTo;
    public int activeCitiesPercentage;

    public GoodAqiStreak() {
    }

    public GoodAqiStreak(int bucketFrom, int bucketTo, int activeCitiesPercentage) {
        this.bucketFrom = bucketFrom;
        this.bucketTo = bucketTo;
        this.activeCitiesPercentage = activeCitiesPercentage;
    }

    public int getBucketFrom() {
        return bucketFrom;
    }

    public void setBucketFrom(int bucketFrom) {
        this.bucketFrom = bucketFrom;
    }

    public int getBucketTo() {
        return bucketTo;
    }

    public void setBucketTo(int bucketTo) {
        this.bucketTo = bucketTo;
    }

    public int getActiveCitiesPercentage() {
        return activeCitiesPercentage;
    }

    public void setActiveCitiesPercentage(int activeCitiesPercentage) {
        this.activeCitiesPercentage = activeCitiesPercentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoodAqiStreak that = (GoodAqiStreak) o;
        return bucketFrom == that.bucketFrom &&
                bucketTo == that.bucketTo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bucketFrom, bucketTo, activeCitiesPercentage);
    }

    @Override
    public String toString() {
        return "GoodAqiStreak{" +
                "bucketFrom=" + bucketFrom +
                ", bucketTo=" + bucketTo +
                ", activeCitiesPercentage=" + activeCitiesPercentage +
                '}';
    }
}
