package hr.tel.fer.processes.watermark;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * This is a {@link AssignerWithPunctuatedWatermarks} used to emit Watermarks that lag behind the element with
 * the maximum timestamp (in event time) seen so far by a fixed amount of time, <code>t_late</code>. This can
 * help reduce the number of elements that are ignored due to lateness when computing the final result for a
 * given window, in the case where we know that elements arrive no later than <code>t_late</code> units of time
 * after the watermark that signals that the system event-time has advanced past their (event-time) timestamp.
 * */
public abstract class BoundedPunctuatedWatermark<T> implements AssignerWithPunctuatedWatermarks<T> {
    @Nullable
    @Override
    public Watermark checkAndGetNextWatermark(T lastElement, long extractedTimestamp) {
        Tuple2<Long, Long> watermarkTuple = extractWatermark(lastElement);

//        if(watermarkTuple.f0 == Long.MAX_VALUE){
//            Tuple2<Long, Long> head = bufferedWatermarks.peek();
//            lastSentBatchId = head.f1;
//            long outWatermark = head.f0;
//            outWatermark = outWatermark > lastSentWatermark ? outWatermark : lastSentWatermark+1;
//            lastSentWatermark = outWatermark;
////            System.out.println("Head id: " + lastSentBatchId);
//            bufferedWatermarks.remove();
//            return new Watermark(outWatermark);
//        }

        if(watermarkTuple != null && watermarkTuple.f1 > lastSentBatchId &&  watermarkTuple.f0 != null && !bufferedWatermarks.contains(watermarkTuple)){
            bufferedWatermarks.add(watermarkTuple);
//            System.out.println(bufferedWatermarks);
        }
        Tuple2<Long, Long> head = bufferedWatermarks.peek();
//        System.out.println(bufferedWatermarks);
        if( head != null && (head.f1 == lastSentBatchId+1) && extractedTimestamp > head.f0){
            lastSentBatchId = head.f1;
            long outWatermark = head.f0;
            outWatermark = outWatermark > lastSentWatermark ? outWatermark : lastSentWatermark+1;
            lastSentWatermark = outWatermark;
//            System.out.println("Head id: " + lastSentBatchId);
            bufferedWatermarks.remove();
            return new Watermark(outWatermark);
        }
        else return null;
    }

//    public Watermark checkAndGetNextWatermark(T lastElement, long extractedTimestamp) {
//        Long watermark = extractWatermark(lastElement);
//        if(watermark != null && watermark > lastReceivedWatermark){
//            long emitWatermark = lastReceivedWatermark;
//            lastReceivedWatermark = watermark;
////            System.out.println("Watermark sent: " + watermark);
//            return new Watermark(emitWatermark);
//        }else return null;
//    }

    public abstract Tuple2<Long, Long> extractWatermark(T element);

    public abstract long extractTimestamp(T element);

    @Override
    public final long extractTimestamp(T element, long previousElementTimestamp) {
        long timestamp = extractTimestamp(element);
        if (timestamp > currentMaxTimestamp) {
            currentMaxTimestamp = timestamp;
        }
        return timestamp;
    }

    private static final long serialVersionUID = 1L;

    /** The current maximum timestamp seen so far. */
    private long currentMaxTimestamp = Long.MIN_VALUE;

    /** The timestamp of the last emitted watermark. */
    private long lastSentWatermark = Long.MIN_VALUE;

    long lastSentBatchId = -1;

    public static class TupleComparator implements Serializable, Comparator<Tuple2<Long, Long>> {

        @Override
        public int compare(Tuple2<Long, Long> o1, Tuple2<Long, Long> o2) {
            return Long.compare(o1.f1, o2.f1);
        }
    }

    private PriorityQueue<Tuple2<Long, Long>> bufferedWatermarks = new PriorityQueue<>(10, new TupleComparator());

    public BoundedPunctuatedWatermark() {
        this.currentMaxTimestamp = Long.MIN_VALUE;
    }
}
