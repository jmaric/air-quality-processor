package hr.tel.fer.processes.watermark;

import hr.tel.fer.measurement.MeasurementCity;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;

public class TimestampsAndWatermarksAssigner {

    public static SingleOutputStreamOperator<Tuple4<MeasurementCity, Boolean, Long, Long>> assignTimestampsMeasurements (DataStream<Tuple4<MeasurementCity, Boolean, Long, Long>> dataStream) {
        return dataStream
                .assignTimestampsAndWatermarks(
                        new BoundedPunctuatedWatermark<Tuple4<MeasurementCity, Boolean, Long, Long>>() {
                            @Override
                            public Tuple2<Long, Long> extractWatermark(Tuple4<MeasurementCity, Boolean, Long, Long> element) {
                                if(element.f2 != 0){//if(element.f1 != 0){
                                    return Tuple2.of(element.f2, element.f3);
                                }
                                return null;
                            }

                            @Override
                            public long extractTimestamp(Tuple4<MeasurementCity, Boolean, Long, Long> element) {
                                long timestamp = element.f0.getMeasurement().getTimestamp();
                                return timestamp;
                            }
                        });
    }
}
