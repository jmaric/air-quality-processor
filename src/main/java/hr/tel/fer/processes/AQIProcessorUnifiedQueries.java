package hr.tel.fer.processes;

import hr.tel.fer.aqi.AqiCity;
import hr.tel.fer.aqi.AqiState;
import hr.tel.fer.measurement.MeasurementCity;
import hr.tel.fer.measurement.SimpleMeasurement;
import hr.tel.fer.settings.ProjectSettings;
import hr.tel.fer.streak.GoodAqiStreak;
import hr.tel.fer.time.TimeUtil;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.io.FileWriter;
import java.io.IOException;

public class AQIProcessorUnifiedQueries extends KeyedProcessFunction<String, Tuple4<MeasurementCity, Boolean, Long, Long>, Tuple4<AqiCity, AqiCity, Integer, Long>> {
    private ValueState<AqiState> currentAqiState;
    private ValueState<AqiState> lastYearAqiState;
    private ValueState<Long> timerRegisteredTimestamp = null;

    public long measurementsLowerBoundDiminutive;
    public long snapshotsLowerBoundDiminutive;

    private static final int GOOD_AQI_UPPER_BOUND = 50000;

    public AQIProcessorUnifiedQueries(long measurementsLowerBound, long snapshotsLowerBound){
        this.measurementsLowerBoundDiminutive = measurementsLowerBound;
        this.snapshotsLowerBoundDiminutive = snapshotsLowerBound;
    }

    @Override
    public void open(Configuration config) {
        ValueStateDescriptor<AqiState> currentAqiStateDescriptor = new ValueStateDescriptor<AqiState>(
                "current-aqi-state",
                TypeInformation.of(new TypeHint<AqiState>() {
                }),
                null
        );
        currentAqiState = getRuntimeContext().getState(currentAqiStateDescriptor);

        ValueStateDescriptor<AqiState> lastYearAqiStateDescriptor = new ValueStateDescriptor<AqiState>(
                "last-year-aqi-state",
                TypeInformation.of(new TypeHint<AqiState>() {
                }),
                null
        );
        lastYearAqiState = getRuntimeContext().getState(lastYearAqiStateDescriptor);

        ValueStateDescriptor<Long> timerRegisteredTimestampDescriptor = new ValueStateDescriptor<Long>(
                "timer-registered-timestamp",
                Types.LONG,
                Long.MIN_VALUE
        );
        timerRegisteredTimestamp = getRuntimeContext().getState(timerRegisteredTimestampDescriptor);
    }


    @Override
    public void processElement(Tuple4<MeasurementCity, Boolean, Long, Long> value, Context ctx, Collector<Tuple4<AqiCity, AqiCity, Integer, Long>> out) throws Exception {
        SimpleMeasurement measurement = value.f0.getMeasurement();
        if(currentAqiState.value() == null){
            this.currentAqiState.update(new AqiState(measurementsLowerBoundDiminutive, snapshotsLowerBoundDiminutive, true));
            this.lastYearAqiState.update(new AqiState(measurementsLowerBoundDiminutive, snapshotsLowerBoundDiminutive, false));
        }

        if(value.f1){
            AqiState currentState = currentAqiState.value();
            currentState.processMeasurement(measurement);
            currentAqiState.update(currentState);
        }else{
            AqiState lastYearState = lastYearAqiState.value();
            lastYearState.processMeasurement(measurement);
            lastYearAqiState.update(lastYearState);
        }

        if(timerRegisteredTimestamp.value() == Long.MIN_VALUE){
            ctx.timerService().registerEventTimeTimer(-1);
            timerRegisteredTimestamp.update(-1L);
        }

    }

    public boolean isActive(long lastTimestamp, long watermark){
        return lastTimestamp > (watermark - 10*TimeUtil.MINUTE_PERIOD_NS);
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext ctx, Collector<Tuple4<AqiCity, AqiCity, Integer, Long>> out) throws IOException {
        AqiState currentAqiStateValue = currentAqiState.value();
        AqiState lastYearAqiStateValue = lastYearAqiState.value();

        long watermark = ctx.timerService().currentWatermark();
        if(watermark >= Long.MAX_VALUE){
            clearAll();
            return;
        }

        boolean active = isActive(currentAqiStateValue.getLastTimestamp(), watermark);

        if(currentAqiStateValue.getLastTimestamp() != Long.MIN_VALUE){
            currentAqiStateValue.processBatch(watermark);
//            if(active) sendGoodQualityStreak(currentAqiStateValue, watermark, ctx);
        }
        if(lastYearAqiStateValue.getLastTimestamp() != Long.MIN_VALUE) {
            lastYearAqiStateValue.processBatch(watermark);
        }

        if (active){
            sendBothQueries(currentAqiStateValue, lastYearAqiStateValue, watermark, ctx, out);
        }

        currentAqiState.update(currentAqiStateValue);
        lastYearAqiState.update(lastYearAqiStateValue);

//        if(ctx.getCurrentKey().equals("Pohlheim")){
//            String path = "src/main/resources/pohlheim_state";
//            write("Current measuremnets: " + currentAqiStateValue.getMeasurements().stream().sorted(Comparator.comparingLong(m -> m.getTimestamp())).collect(Collectors.toList()).toString() +
//                            "\nCurrent snapshots: " + currentAqiStateValue.getSnapshots().stream().sorted(Comparator.comparingLong(m -> m.getTimestamp())).collect(Collectors.toList()).toString() +
//                            "\nc aqiP1: " + currentAqiStateValue.getCurrentAqiP1() +
//                            "\nc aqiP2: " + currentAqiStateValue.getCurrentAqiP2() +
//                            "\nc activeAQI: " + currentAqiStateValue.getActiveAQI() +
//                            "\nc averageAQI" + currentAqiStateValue.getAverageAQI() +
//            "\nLY measuremnets: " + lastYearAqiStateValue.getMeasurements().stream().sorted(Comparator.comparingLong(m -> m.getTimestamp())).collect(Collectors.toList()).toString() +
//                            "\nLY snapshots: " + lastYearAqiStateValue.getSnapshots().stream().map(ind -> new AirQualityIndex(ind.getTimestamp()-TimeUtil.YEAR_PERIOD_NS, ind.getAqi()))
//                            .sorted(Comparator.comparingLong(m -> m.getTimestamp())).collect(Collectors.toList()).toString() +
//                            "\nl aqiP1: " + lastYearAqiStateValue.getCurrentAqiP1() +
//                            "\nl aqiP2: " + lastYearAqiStateValue.getCurrentAqiP2() +
//                            "\nl activeAQI: " + lastYearAqiStateValue.getActiveAQI() +
//                            "\nl averageAQI" + lastYearAqiStateValue.getAverageAQI()
//                    , path + "/aqi-state-stockelsdorf-" + watermark/1000000000 + ".txt");
//        }

        ctx.timerService().registerEventTimeTimer(watermark+1);
        timerRegisteredTimestamp.update(watermark+1);
    }

    public void sendGoodQualityStreak(AqiState currentAqiState, long watermark, OnTimerContext ctx) throws IOException {
        Long goodSinceTimestamp = currentAqiState.getGoodSinceTimestamp();
        if(goodSinceTimestamp == null) return;

        int goodFor = (int)((watermark - goodSinceTimestamp)/1E9);//in seconds
        if(goodFor < 0) return;
        goodFor = (goodFor > 7*TimeUtil.DAY_PERIOD_SEC) ? 7*TimeUtil.DAY_PERIOD_SEC : goodFor;
        ctx.output(ProjectSettings.goodAirQualityOutputTag, Tuple2.of(goodFor, watermark));
    }

    public Integer getGoodQualityStreak(AqiState currentAqiState, long watermark, OnTimerContext ctx){
        Long goodSinceTimestamp = currentAqiState.getGoodSinceTimestamp();
        if(goodSinceTimestamp == null) return -1;

        int goodFor = (int)((watermark - goodSinceTimestamp)/1E9);//in seconds
        if(goodFor < 0) return -1;
        goodFor = (goodFor > 7*TimeUtil.DAY_PERIOD_SEC) ? 7*TimeUtil.DAY_PERIOD_SEC : goodFor;
        return goodFor;
    }

    public void sendBothQueries(AqiState currentAqiStateValue, AqiState lastYearAqiStateValue,
                             long watermark, OnTimerContext ctx, Collector<Tuple4<AqiCity, AqiCity, Integer, Long>> out) {
//        long outWatermark = watermark + (currentMeasurements ? 0 : TimeUtil.YEAR_PERIOD_NS);
        if(currentAqiStateValue.getAverageAQI() != null && lastYearAqiStateValue.getAverageAQI() != null) {
            AqiCity aqiCityCurrent = AqiCity.of(
                    ctx.getCurrentKey(),
                    currentAqiStateValue.getAverageAQI(),
                    currentAqiStateValue.getCurrentAqiP1(),
                    currentAqiStateValue.getCurrentAqiP2());
            AqiCity aqiCityLastYear = AqiCity.of(
                    ctx.getCurrentKey(),
                    lastYearAqiStateValue.getAverageAQI(),
                    lastYearAqiStateValue.getCurrentAqiP1(),
                    lastYearAqiStateValue.getCurrentAqiP2());
//            System.out.println("Seinding tup: " + Tuple3.of(
//                    aqiCityCurrent,
//                    aqiCityLastYear,
//                    watermark));
            out.collect(Tuple4.of(
                    aqiCityCurrent,
                    aqiCityLastYear,
                    getGoodQualityStreak(currentAqiStateValue, watermark, ctx),
                    watermark));
//            if(currentMeasurements && ctx.getCurrentKey().equals("Markdorf")){
//                System.out.println("[" + outWatermark + "]Current m: " + aqiCity);
//            }
//            if(!currentMeasurements && ctx.getCurrentKey().equals("Markdorf")){
//                System.out.println("[" + outWatermark + "]LastY m: " + aqiCity);
//            }
        }
    }

    public static void write(String str, String path){
        try {
            FileWriter myWriter = new FileWriter(path, true);

            try{myWriter.write(str+"\n");}
            catch(IOException ex){
                throw new RuntimeException(ex);
            }

            myWriter.close();
//            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void clearAll(){
        currentAqiState.clear();
        lastYearAqiState.clear();
        timerRegisteredTimestamp.clear();

    }

}
