package hr.tel.fer.processes;

import hr.tel.fer.aqi.AqiCity;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.KeyedCoProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Query1ResponserUnified extends KeyedCoProcessFunction<Long, Tuple3<AqiCity, AqiCity, Long>, Tuple3<Long, Long, Integer>, Tuple2<List<AqiCity>,Long>> {
    private ValueState<Map<String, Tuple2<AqiCity, AqiCity>>> aqiCityPairs;
    private ValueState<Long> timerRegisteredTimestamp = null;

    private ValueState<Long> batchId;

    private static final int MIN_ELEMENTS = 50;//50

    private static final long PROLONG_PERIOD_MIN = 50;// TODO was 100
    private static final long PROLONG_PERIOD_MAX = 3500;
    private static final long TIME_TO_LIVE = 10000;
    private ValueState<Long> activeProlongPeriod;
    private ValueState<Integer> currentElementsCounter;

    private static final Logger LOG = LoggerFactory.getLogger(Query1ResponserUnified.class);

    @Override
    public void open(Configuration config) {
//        MapStateDescriptor<String, Tuple2<AqiCity, AqiCity>> aqiCityPairsDescriptor = new MapStateDescriptor<String, Tuple2<AqiCity, AqiCity>>(
//                // state name
//                "aqi-city-pairs",
//                Types.STRING,
//                // type information of elements
//                TypeInformation.of(new TypeHint<Tuple2<AqiCity, AqiCity>>() {
//                }));

        ValueStateDescriptor<Map<String, Tuple2<AqiCity, AqiCity>>> aqiCityPairsDescriptor = new ValueStateDescriptor<Map<String, Tuple2<AqiCity, AqiCity>>>(
                // state name
                "aqi-city-pairs",
                // type information of elements
                TypeInformation.of(new TypeHint<Map<String, Tuple2<AqiCity, AqiCity>>>() {
                }));
        this.aqiCityPairs = getRuntimeContext().getState(aqiCityPairsDescriptor);

        ValueStateDescriptor<Long> timerRegisteredTimestampDescriptor = new ValueStateDescriptor<Long>(
                "timer-registered-timestamp",
                Types.LONG,
                Long.MIN_VALUE
        );
        timerRegisteredTimestamp = getRuntimeContext().getState(timerRegisteredTimestampDescriptor);

        ValueStateDescriptor<Long> batchIdDescriptor = new ValueStateDescriptor<Long>(
                "batch-id",
                Types.LONG,
                null
        );
        batchId = getRuntimeContext().getState(batchIdDescriptor);

        ValueStateDescriptor<Integer> currentElementsCounterDescriptor = new ValueStateDescriptor<Integer>(
                "current-elements-counter",
                Types.INT,
                0
        );
        currentElementsCounter = getRuntimeContext().getState(currentElementsCounterDescriptor);

        ValueStateDescriptor<Long> activeProlongPeriodDescriptor = new ValueStateDescriptor<Long>(
                "active-prolong-period",
                Types.LONG,
                PROLONG_PERIOD_MAX
        );
        activeProlongPeriod = getRuntimeContext().getState(activeProlongPeriodDescriptor);

    }


    @Override
    public void processElement2(Tuple3<Long, Long, Integer> value, Context ctx, Collector<Tuple2<List<AqiCity>, Long>> out) throws Exception {
        if(batchId.value() == null){
            batchId.update(value.f0);
            if(timerRegisteredTimestamp.value() == Long.MIN_VALUE){
                long triggerTimestamp = ctx.timerService().currentProcessingTime() + TIME_TO_LIVE;
                ctx.timerService().registerProcessingTimeTimer(triggerTimestamp);
                timerRegisteredTimestamp.update(triggerTimestamp);
            }else if(timerRegisteredTimestamp.value() < ctx.timerService().currentProcessingTime()){
                long triggerTimestamp = ctx.timerService().currentProcessingTime() + 10;
                ctx.timerService().registerProcessingTimeTimer(triggerTimestamp);
                timerRegisteredTimestamp.update(triggerTimestamp);
            }
//            else if(timerRegisteredTimestamp.value() > ctx.timerService().currentProcessingTime()){
//                long triggerTimestamp = ctx.timerService().currentProcessingTime() + PROLONG_PERIOD_MIN;
//                ctx.timerService().registerProcessingTimeTimer(triggerTimestamp);
//                timerRegisteredTimestamp.update(triggerTimestamp);
//            }
        }
    }

    @Override
    public void processElement1(Tuple3<AqiCity, AqiCity, Long> value, Context ctx, Collector<Tuple2<List<AqiCity>,Long>> out) throws Exception {
        if(currentElementsCounter.value() >= MIN_ELEMENTS){
            long triggerTimestamp = ctx.timerService().currentProcessingTime() + activeProlongPeriod.value();
            activeProlongPeriod.update(Math.max((int)(activeProlongPeriod.value()*0.995), PROLONG_PERIOD_MIN));
            ctx.timerService().deleteProcessingTimeTimer(timerRegisteredTimestamp.value());
            ctx.timerService().registerProcessingTimeTimer(triggerTimestamp);
            timerRegisteredTimestamp.update(triggerTimestamp);
        }

        AqiCity aqiCityCurr = value.f0;
        AqiCity aqiCityLy = value.f1;

        Map<String, Tuple2<AqiCity, AqiCity>> aqiCityPairsMap = aqiCityPairs.value();

        if(aqiCityPairsMap == null){
            aqiCityPairsMap = new HashMap<>();
        }

        currentElementsCounter.update(currentElementsCounter.value()+1);

        aqiCityPairsMap.put(aqiCityCurr.getCity(), Tuple2.of(aqiCityCurr, aqiCityLy));
        aqiCityPairs.update(aqiCityPairsMap);
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext ctx, Collector<Tuple2<List<AqiCity>,Long>> out) throws IOException {
        Map<String, Tuple2<AqiCity,AqiCity>> pairs = aqiCityPairs.value();
        List<AqiCity> aqiCities;

//        System.out.println("BID: " + batchId.value());

        if(batchId.value() == null){
            return;
        }else if(pairs == null){
            aqiCities = new ArrayList<>();
        }else{
            aqiCities = aqiCityPairs.value().entrySet().stream().map(entry -> entry.getValue())
                    .filter(tup -> tup.f0 != null && tup.f1 != null)
                    .map(tuple -> {
                        AqiCity current = tuple.f0;
                        AqiCity lastYear = tuple.f1;
                        current.setAqi(-(current.getAqi()-lastYear.getAqi()));
                        return current;
                    }).sorted(Comparator.comparingInt(AqiCity::getAqi).reversed())
                    .limit(50)
                    .collect(Collectors.toList());
        }

        out.collect(Tuple2.of(aqiCities, batchId.value()));
        if(batchId.value()%1000==0 || batchId.value()==29999) {
//            System.out.println("Current elements num: " + currentElementsCounter.value());
            System.out.println("[" + batchId.value() + "]Q1 out:" + aqiCities);
        }
        if(aqiCities.size() == 0){
            System.out.println("[" + batchId.value() + "]Zero measurements: " + aqiCityPairs.value());
        }

        aqiCityPairs.clear();
        timerRegisteredTimestamp.clear();
        batchId.clear();
        activeProlongPeriod.clear();
        currentElementsCounter.clear();
    }
}
