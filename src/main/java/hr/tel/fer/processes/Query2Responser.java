package hr.tel.fer.processes;

import hr.tel.fer.streak.GoodAqiStreak;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.KeyedCoProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Query2Responser extends KeyedCoProcessFunction<Long, Tuple2<Integer, Long>, Tuple3<Long, Long, Integer>, Tuple2<List<GoodAqiStreak>,Long>> {

    private static final Logger LOG = LoggerFactory.getLogger(Query2Responser.class);
    private ValueState<List<Integer>> goodAqiStreaks;
    private ValueState<Long> timerRegisteredTimestamp = null;

    private ValueState<Tuple2<Long, Integer>> batchIdLengthPair;

    private static final int MIN_ELEMENTS = 50;//50

    private static final long PROLONG_PERIOD_MIN = 50;
    private static final long PROLONG_PERIOD_MAX = 10000;
    private ValueState<Long> activeProlongPeriod;
    private ValueState<Integer> currentElementsCounter;
    @Override
    public void open(Configuration config) {
//        MapStateDescriptor<String, Tuple2<AqiCity, AqiCity>> aqiCityPairsDescriptor = new MapStateDescriptor<String, Tuple2<AqiCity, AqiCity>>(
//                // state name
//                "aqi-city-pairs",
//                Types.STRING,
//                // type information of elements
//                TypeInformation.of(new TypeHint<Tuple2<AqiCity, AqiCity>>() {
//                }));

        ValueStateDescriptor<List<Integer>> goodAqiStreaksDescriptor = new ValueStateDescriptor<List<Integer>>(
                // state name
                "good-aqi-streaks-2",
                // type information of elements
                TypeInformation.of(new TypeHint<List<Integer>>() {
                }));
        this.goodAqiStreaks = getRuntimeContext().getState(goodAqiStreaksDescriptor);

        ValueStateDescriptor<Long> timerRegisteredTimestampDescriptor = new ValueStateDescriptor<Long>(
                "timer-registered-timestamp-2",
                Types.LONG,
                Long.MIN_VALUE
        );
        timerRegisteredTimestamp = getRuntimeContext().getState(timerRegisteredTimestampDescriptor);

        ValueStateDescriptor<Tuple2<Long, Integer>> batchIdLengthPairDescriptor = new ValueStateDescriptor<Tuple2<Long, Integer>>(
                "batch-id-2",
                TypeInformation.of(new TypeHint<Tuple2<Long,Integer>>() {}),
                null
        );
        batchIdLengthPair = getRuntimeContext().getState(batchIdLengthPairDescriptor);

        ValueStateDescriptor<Integer> currentElementsCounterDescriptor = new ValueStateDescriptor<Integer>(
                "current-elements-counter-2",
                Types.INT,
                0
        );
        currentElementsCounter = getRuntimeContext().getState(currentElementsCounterDescriptor);

        ValueStateDescriptor<Long> activeProlongPeriodDescriptor = new ValueStateDescriptor<Long>(
                "active-prolong-period-2",
                Types.LONG,
                3500L
        );
        activeProlongPeriod = getRuntimeContext().getState(activeProlongPeriodDescriptor);

    }


    @Override
    public void processElement2(Tuple3<Long, Long, Integer> value, Context ctx, Collector<Tuple2<List<GoodAqiStreak>, Long>> out) throws Exception {
        if(batchIdLengthPair.value() == null){
            batchIdLengthPair.update(Tuple2.of(value.f0, value.f2));
            if(timerRegisteredTimestamp.value() == Long.MIN_VALUE){
                long triggerTimestamp = ctx.timerService().currentProcessingTime() + PROLONG_PERIOD_MAX;
                ctx.timerService().registerProcessingTimeTimer(triggerTimestamp);
                timerRegisteredTimestamp.update(triggerTimestamp);
            }else if(timerRegisteredTimestamp.value() < ctx.timerService().currentProcessingTime()){
                long triggerTimestamp = ctx.timerService().currentProcessingTime() + 10;
                ctx.timerService().registerProcessingTimeTimer(triggerTimestamp);
                timerRegisteredTimestamp.update(triggerTimestamp);
            }
        }
    }

    @Override
    public void processElement1(Tuple2<Integer, Long> value, Context ctx, Collector<Tuple2<List<GoodAqiStreak>,Long>> out) throws Exception {
        if(currentElementsCounter.value() >= MIN_ELEMENTS){
            long triggerTimestamp = ctx.timerService().currentProcessingTime() + activeProlongPeriod.value();
            activeProlongPeriod.update(Math.max((int)(activeProlongPeriod.value()*0.995), PROLONG_PERIOD_MIN));
            ctx.timerService().deleteProcessingTimeTimer(timerRegisteredTimestamp.value());
            ctx.timerService().registerProcessingTimeTimer(triggerTimestamp);
            timerRegisteredTimestamp.update(triggerTimestamp);
        }

        currentElementsCounter.update(currentElementsCounter.value()+1);

        Integer streak = value.f0;

        List<Integer> goodAqiStreaksList = goodAqiStreaks.value();

        if(goodAqiStreaksList == null){
            goodAqiStreaksList = new ArrayList<>();
        }

        goodAqiStreaksList.add(streak);
        goodAqiStreaks.update(goodAqiStreaksList);
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext ctx, Collector<Tuple2<List<GoodAqiStreak>,Long>> out) throws IOException {
        if(batchIdLengthPair.value() == null){
            return;
        }

        List<Integer> lengths = goodAqiStreaks.value();
        List<GoodAqiStreak> streaks;

        Long batchId = batchIdLengthPair.value().f0;
        Integer maxLength = batchIdLengthPair.value().f1;

        if(lengths == null || batchId == null || maxLength == null){
            streaks = new ArrayList<>();
        }
        else{
            double partOfMaxLength = maxLength*1.0/14;
            int[] bucketRanges = new int[15];
            List<GoodAqiStreak> emptyStreaks = new ArrayList<>();

            for(int i = 0; i < 15; i+=1){
                bucketRanges[i] = (int)Math.round(i*partOfMaxLength);
            }
            NavigableMap<Integer, Integer> map = new TreeMap<>();
            for(int i = 0; i < 14; i+=1){
                map.put(bucketRanges[i], i);
                emptyStreaks.add(new GoodAqiStreak(bucketRanges[i], (i != 13) ? bucketRanges[i+1]-1 : maxLength, 0));
            }
            int size = lengths.size();
//            System.out.println("Lengths: " + lengths);

            streaks = lengths.stream()
                    .collect(Collectors.groupingBy(length -> getIndexForRange(map, length, maxLength), Collectors.counting()))
                    .entrySet().stream()
                    .sorted(Comparator.comparingInt(e -> e.getKey()))
                    .map(entry -> new GoodAqiStreak(
                            bucketRanges[entry.getKey()],
                            (entry.getKey() != 13) ? bucketRanges[entry.getKey()+1]-1 : maxLength,
                            (int)(Math.round(1.0*entry.getValue()/size * 100.0)))
                    ).collect(Collectors.toList());
            emptyStreaks.removeAll(streaks);
            streaks.addAll(emptyStreaks);
            streaks = streaks.stream().sorted(Comparator.comparingInt(s -> s.getBucketFrom())).collect(Collectors.toList());
        }

        if(lengths == null || lengths.size() == 0){
            LOG.info("[" + ctx.getCurrentKey() + "]Zero2 measurements: " + lengths);
        }

        if(batchId%1000==0 || batchId==29999) {
//            System.out.println("Current elements num: " + currentElementsCounter.value());
            System.out.println("[" + batchId + "]Q2 out:" + streaks);
        }

        out.collect(Tuple2.of(streaks, batchId));

        goodAqiStreaks.clear();
        timerRegisteredTimestamp.clear();
        batchIdLengthPair.clear();
        activeProlongPeriod.clear();
        currentElementsCounter.clear();

    }

    public static int getIndexForRange(NavigableMap<Integer, Integer> map, int length, int maxLength){
//        System.out.println("Map: " + map);
//        System.out.println("Length: " + length);
        if (length > maxLength) {
            return 13;
        } else {
            try{
                return map.floorEntry(length).getValue();
            }catch(NullPointerException ex) {
                System.out.println("Map: " + map);
                System.out.println("Length: " + length);
                throw new RuntimeException(ex);
            }
        }
    }
}
