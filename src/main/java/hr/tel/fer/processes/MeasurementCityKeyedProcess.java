package hr.tel.fer.processes;

import de.tum.i13.proto.Location;
import hr.tel.fer.city.CityFinder;
import hr.tel.fer.city.CityFinderSeq;
import hr.tel.fer.measurement.InputMeasurement;
import hr.tel.fer.measurement.MeasurementCity;
import hr.tel.fer.measurement.SimpleMeasurement;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.util.List;

public class MeasurementCityKeyedProcess extends KeyedProcessFunction<Integer, Tuple2<InputMeasurement, Integer>, Tuple4<MeasurementCity, Boolean, Long, Long>> {

    private CityFinder cityFinder;

    public MeasurementCityKeyedProcess(List<Location> locations){
        this.cityFinder = new CityFinderSeq(locations);
    }

    public MeasurementCityKeyedProcess(CityFinder cityFinder){
        this.cityFinder = cityFinder;
    }

    @Override
    public void processElement(Tuple2<InputMeasurement, Integer> value, Context ctx, Collector<Tuple4<MeasurementCity, Boolean, Long, Long>> out) throws Exception {
        InputMeasurement inputMeasurement = value.f0;
        String city = cityFinder.getCityFromPoint(inputMeasurement.getLongitude(), inputMeasurement.getLatitude());
        if(city != null) {
            out.collect(Tuple4.of(new MeasurementCity(
                            new SimpleMeasurement(inputMeasurement.getTimestamp(),
                                    inputMeasurement.getP1(), inputMeasurement.getP2()), city), inputMeasurement.getCurrent(),
                    inputMeasurement.getCurrentWatermark() == null ? 0 : inputMeasurement.getCurrentWatermark(), inputMeasurement.getBatchSeqId()));

        }
    }
}
