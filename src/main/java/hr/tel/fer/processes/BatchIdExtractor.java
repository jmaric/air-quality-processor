package hr.tel.fer.processes;

import hr.tel.fer.measurement.InputMeasurement;
import hr.tel.fer.time.TimeUtil;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class BatchIdExtractor extends KeyedProcessFunction<String, InputMeasurement, Tuple3<Long, Long, Integer>> {

    private ValueState<Long> firstTimestamp;
    private ValueState<Boolean> passed7Days;
    private ValueState<PriorityQueue<Tuple2<Long, Long>>> buffered;
    private ValueState<Long> lastSentIndex;
    private ValueState<Long> lastSentWatermark;
//    private ListState<Tuple2<Long, Long>> buffered;

    @Override
    public void open(Configuration config) {
        ValueStateDescriptor<Long> firstTimestampDescriptor = new ValueStateDescriptor<Long>(
                "first-timestamp-bie",
                Types.LONG,
                null
        );
        firstTimestamp = getRuntimeContext().getState(firstTimestampDescriptor);

        ValueStateDescriptor<Long> lastSentWatermarkDescriptor = new ValueStateDescriptor<Long>(
                "last-sent-watermark-bie",
                Types.LONG,
                Long.MIN_VALUE
        );
        lastSentWatermark = getRuntimeContext().getState(lastSentWatermarkDescriptor);

        ValueStateDescriptor<Long> lastSentIndexDescriptor = new ValueStateDescriptor<Long>(
                "last-sent-index",
                Types.LONG,
                -1L
        );
        lastSentIndex = getRuntimeContext().getState(lastSentIndexDescriptor);

        ValueStateDescriptor<Boolean> passed7DaysDescriptor = new ValueStateDescriptor<Boolean>(
                "passed-7-days",
                Types.BOOLEAN,
                false
        );
        passed7Days = getRuntimeContext().getState(passed7DaysDescriptor);

        ValueStateDescriptor<PriorityQueue<Tuple2<Long, Long>>> bufferedDescriptor = new ValueStateDescriptor<>(
                // state name
                "buffered-bie",
                // type information of state
                TypeInformation.of(new TypeHint<PriorityQueue<Tuple2<Long, Long>>>() {
                }),
                null);
        buffered = getRuntimeContext().getState(bufferedDescriptor);

//        ListStateDescriptor<Tuple2<Long, Long>> bufferedDescriptor = new ListStateDescriptor<Tuple2<Long, Long>>(
//                "buffered-batch-ids",
//                TypeInformation.of(new TypeHint<Tuple2<Long, Long>>() {}
//        ));
//        buffered = getRuntimeContext().getListState(bufferedDescriptor);
    }


    @Override
    public void processElement(InputMeasurement im, Context ctx, Collector<Tuple3<Long, Long, Integer>> out) throws Exception {
//        if(im.getBatchSeqId() == Long.MAX_VALUE){
//            if(im.getCurrent()){
//                ctx.output(ProjectSettings.currentExtraWatermarksOutputTag, Tuple2.of(new MeasurementCity(new SimpleMeasurement(im.getCurrentWatermark(), 100001F, 100001F), null), im.getCurrentWatermark()));
//            }else if(!im.getCurrent()){
//                ctx.output(ProjectSettings.lastYearExtraWatermarksOutputTag, Tuple2.of(new MeasurementCity(new SimpleMeasurement(im.getCurrentWatermark(), 100001F, 100001F), null), im.getCurrentWatermark()));
//            }
//            return;
//        }

        if(buffered.value() == null){
            buffered.update(new PriorityQueue<Tuple2<Long, Long>>(10, Comparator.comparing(tup -> tup.f0)));
        }

        if(im.getBatchSeqId() == 0){
            firstTimestamp.update(im.getBatchFirstTimestamp());
        }

        if(lastSentIndex.value()+1 != im.getBatchSeqId()){
            PriorityQueue<Tuple2<Long, Long>> queue = buffered.value();
            queue.add(Tuple2.of(im.getBatchSeqId(), im.getCurrentWatermark()));
            buffered.update(queue);
        }else{
            sendBatchId(im.getBatchSeqId(), im.getCurrentWatermark(), out, ctx);
            sendBuffered(out, ctx);
        }

//        sendBatchId(im.getBatchSeqId(), im.getCurrentWatermark(), out);
    }

    private void sendBatchId(long batchSeqId, long currentWatermark, Collector<Tuple3<Long, Long, Integer>> out, Context ctx)
            throws IOException {
        lastSentIndex.update(lastSentIndex.value()+1);
        long lastSentWatermarkValue = lastSentWatermark.value();
        currentWatermark = currentWatermark > lastSentWatermark.value() ? currentWatermark : lastSentWatermarkValue+1;
        lastSentWatermark.update(currentWatermark);
//        System.out.println("BIE: " + Tuple2.of(batchSeqId, currentWatermark));

        if(passed7Days.value()) {
            out.collect(Tuple3.of(batchSeqId, currentWatermark, 7*TimeUtil.DAY_PERIOD_SEC));
            return;
        }

        int goodAirQualityMax = (int)((currentWatermark - firstTimestamp.value())/1E9);
        if(goodAirQualityMax > 7*TimeUtil.DAY_PERIOD_SEC){
            goodAirQualityMax = 7*TimeUtil.DAY_PERIOD_SEC;
            passed7Days.update(true);
        }
//        System.out.println("BID: " + batchSeqId);
//        System.out.println("Buffered: " + buffered.value());
//        ctx.output(ProjectSettings.currentExtraWatermarksOutputTag, Tuple2.of(new MeasurementCity(new SimpleMeasurement(currentWatermark, 100001F, 100001F), null), currentWatermark));
//        ctx.output(ProjectSettings.lastYearExtraWatermarksOutputTag, Tuple2.of(new MeasurementCity(new SimpleMeasurement(currentWatermark, 100001F, 100001F), null), currentWatermark));
        out.collect(Tuple3.of(batchSeqId, currentWatermark, goodAirQualityMax));
    }

    private void sendBuffered(Collector<Tuple3<Long, Long, Integer>> out, Context ctx) throws IOException {
        PriorityQueue<Tuple2<Long, Long>> queue = buffered.value();
        Tuple2<Long,Long> head = queue.peek();
        while(head != null && head.f0 == lastSentIndex.value()+1){
            sendBatchId(head.f0, head.f1, out, ctx);
//                pw.println(head.f0.getMeasurement().getTimestamp());
            queue.remove(head);
            head = queue.peek();
        }
    }
}