package hr.tel.fer.factories;

import de.tum.i13.grpc.ChallengerGrpc;
import de.tum.i13.proto.*;
import hr.tel.fer.settings.ProjectSettings;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class StubManager implements Serializable {
    public static ChallengerGrpc.ChallengerBlockingStub stub =
            ChallengerGrpc.newBlockingStub(ManagedChannelBuilder
            .forAddress(ProjectSettings.EXTERNAL_CHANNEL_ADDRESS, 5023)
            .usePlaintext()
            .build()) //for demo, we show the blocking stub
                .withMaxInboundMessageSize(100 * 1024 * 1024)
                .withMaxOutboundMessageSize(100 * 1024 * 1024);

    public static List<Location> getLocations(Benchmark benchmark) {
        return stub.getLocations(benchmark).getLocationsList();
    }

    public static Benchmark createGCBenchmark(String type){
        return StubManager.createNewBenchmark(BenchmarkConfiguration.newBuilder()
                .setBenchmarkName("Testrun " + new Date().toString())
                .setBatchSize(10000)
                .addQueries(BenchmarkConfiguration.Query.Q1)
                .addQueries(BenchmarkConfiguration.Query.Q2)
                .setToken("wikhdybkyxrksrftqwytuikdryrlbnqc") //go to: https://challenge.msrg.in.tum.de/profile/
                .setBenchmarkType(type) //Benchmark Type for testing
                .build());
    }

    public StubManager(String address){
        System.out.println("Initializing SM,");
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress(address, 5023)
                .usePlaintext()
                .build();

        stub = ChallengerGrpc.newBlockingStub(channel) //for demo, we show the blocking stub
                .withMaxInboundMessageSize(100 * 1024 * 1024)
                .withMaxOutboundMessageSize(100 * 1024 * 1024);
    }

    public static void initializeStub(String address){
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress(address, 5023)
                .usePlaintext()
                .build();

        stub = ChallengerGrpc.newBlockingStub(channel) //for demo, we show the blocking stub
                .withMaxInboundMessageSize(100 * 1024 * 1024)
                .withMaxOutboundMessageSize(100 * 1024 * 1024);

    }

    public static void resultQ1(ResultQ1 resultQ1){
        try{
            stub.resultQ1(resultQ1);
        }catch(StatusRuntimeException ex){
            ex.printStackTrace();
        }
    }

    public static void resultQ2(ResultQ2 resultQ2){
        try{
            stub.resultQ2(resultQ2);
        }catch(StatusRuntimeException ex){
            ex.printStackTrace();
        }
    }

    public static Benchmark createNewBenchmark(BenchmarkConfiguration bc){
        return stub.createNewBenchmark(bc);
    }

    public static void startBenchmark(Benchmark benchmark){
        try{
            stub.startBenchmark(benchmark);
        }catch(StatusRuntimeException ex){
            ex.printStackTrace();
        }
    }

    public static void endBenchmark(Benchmark benchmark){
        try{
            stub.endBenchmark(benchmark);
        }catch(StatusRuntimeException ex){
            ex.printStackTrace();
        }

    }

    public static Batch nextBatch(Benchmark benchmark){
        return stub.nextBatch(benchmark);
    }

    public ChallengerGrpc.ChallengerBlockingStub getStub() {
        return stub;
    }

    public void setStub(ChallengerGrpc.ChallengerBlockingStub stub) {
        this.stub = stub;
    }
}
