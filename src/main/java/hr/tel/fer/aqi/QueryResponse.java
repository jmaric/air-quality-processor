package hr.tel.fer.aqi;

import hr.tel.fer.streak.GoodAqiStreak;

import java.util.List;
import java.util.Objects;

public class QueryResponse {
    private List<AqiCity> topKCities;
    private List<GoodAqiStreak> topKStreaks;
    private long batchId;

    public QueryResponse(){

    }

    public QueryResponse(List<AqiCity> topKCities, List<GoodAqiStreak> topKStreaks, long batchId) {
        this.topKCities = topKCities;
        this.topKStreaks = topKStreaks;
        this.batchId = batchId;
    }

    public List<AqiCity> getTopKCities() {
        return topKCities;
    }

    public void setTopKCities(List<AqiCity> topKCities) {
        this.topKCities = topKCities;
    }

    public List<GoodAqiStreak> getTopKStreaks() {
        return topKStreaks;
    }

    public void setTopKStreaks(List<GoodAqiStreak> topKStreaks) {
        this.topKStreaks = topKStreaks;
    }

    public long getBatchId() {
        return batchId;
    }

    public void setBatchId(long batchId) {
        this.batchId = batchId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryResponse that = (QueryResponse) o;
        return batchId == that.batchId &&
                Objects.equals(topKCities, that.topKCities) &&
                Objects.equals(topKStreaks, that.topKStreaks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topKCities, topKStreaks, batchId);
    }
}
