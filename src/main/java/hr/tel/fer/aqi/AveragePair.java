package hr.tel.fer.aqi;

import java.util.Objects;

public class AveragePair {
    public double sum;
    public int count;

    public AveragePair(){
        sum = 0;
        count = 0;
    }

    public AveragePair addElement(double element){
        sum += element;
        count++;
        return this;
    }

    public AveragePair removeElement(double element){
        sum -= element;
        count--;
        return this;
    }

    public AveragePair(double sum, int count) {
        this.sum = sum;
        this.count = count;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AveragePair that = (AveragePair) o;
        return sum == that.sum &&
                count == that.count;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sum, count);
    }

    @Override
    public String toString() {
        return "AveragePair{" +
                "sum=" + sum +
                ", count=" + count +
                '}';
    }
}


