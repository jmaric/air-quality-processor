package hr.tel.fer.aqi;

import java.util.Objects;

public class AirQualityIndex {
    private long timestamp;
    private int aqi;

    public AirQualityIndex(){

    }

    public AirQualityIndex(long timestamp, int aqi) {
        this.timestamp = timestamp;
        this.aqi = aqi;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getAqi() {
        return aqi;
    }

    public void setAqi(int aqi) {
        this.aqi = aqi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AirQualityIndex that = (AirQualityIndex) o;
        return timestamp == that.timestamp &&
                aqi == that.aqi;
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, aqi);
    }

    @Override
    public String toString() {
        return timestamp/1000000000 +
                ";" + aqi;
    }
}
