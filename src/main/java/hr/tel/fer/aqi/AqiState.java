package hr.tel.fer.aqi;

import com.thanglequoc.aqicalculator.AQICalculator;
import com.thanglequoc.aqicalculator.Pollutant;
import hr.tel.fer.measurement.SimpleMeasurement;
import hr.tel.fer.time.TimeUtil;

import java.util.Comparator;
import java.util.Objects;
import java.util.OptionalDouble;
import java.util.PriorityQueue;

public class AqiState {
    private PriorityQueue<SimpleMeasurement> measurements;
    private PriorityQueue<AirQualityIndex> snapshots;
    private Integer currentAqiP1;
    private Integer currentAqiP2;
    private Integer activeAQI;
    private Integer averageAQI;
    private AveragePair p1AveragePair;
    private AveragePair p2AveragePair;
    private AveragePair snapshotsAveragePair;
    private Long lastTimestamp;
    private Long nextSnapshotTime;
    private Long goodSinceTimestamp = null;
    private long measurementsLowerBoundDiminutive;
    private long snapshotsLowerBoundDiminutive;
    private boolean currentMeasurements;

    private static final int GOOD_AQI_UPPER_BOUND = 50000;

    public AqiState() {

    }

    public AqiState(long measurementsLowerBound, long snapshotsLowerBound, boolean currentMeasurements) {
        measurements = new PriorityQueue<>(10, Comparator.comparing(meas -> meas.getTimestamp()));
        snapshots = new PriorityQueue<>(10, Comparator.comparing(aqi -> aqi.getTimestamp()));
        nextSnapshotTime = Long.MIN_VALUE;
        lastTimestamp = Long.MIN_VALUE;
        this.measurementsLowerBoundDiminutive = measurementsLowerBound;
        this.snapshotsLowerBoundDiminutive = snapshotsLowerBound;
        this.currentMeasurements = currentMeasurements;
        p1AveragePair = new AveragePair();
        p2AveragePair = new AveragePair();
        snapshotsAveragePair = new AveragePair();
    }

    public void processBatch(long watermark) {
        if (watermark >= nextSnapshotTime) {
            updateAndCheckForSnapshot(watermark);
            removeSnapshotsWithWatermark(watermark);
            updateAverageAqi(watermark);
        }
        removeMeasurementsWithWatermark(watermark);
        updateAqi(watermark);
    }

    public void processMeasurement(SimpleMeasurement measurement) {
        Long timestamp = measurement.getTimestamp();
        if (nextSnapshotTime == Long.MIN_VALUE) {
            nextSnapshotTime = TimeUtil.getNext5MinuteFromNs(timestamp);
        }

        updateAndCheckForSnapshot(timestamp);

        lastTimestamp = timestamp;

        addMeasurement(measurement);
    }

    public void processMeasurement(SimpleMeasurement measurement, long maxCurrentTimestamp) {
        Long timestamp = measurement.getTimestamp();
        if (nextSnapshotTime == Long.MIN_VALUE) {
            nextSnapshotTime = TimeUtil.getNext5MinuteFromNs(timestamp);
        }

        lastTimestamp = timestamp;

        addMeasurement(measurement);

        updateAndCheckForSnapshot(maxCurrentTimestamp);
    }

    public void addMeasurement(SimpleMeasurement measurement) {
        measurements.add(measurement);
        p1AveragePair.addElement(measurement.getP1());
        p2AveragePair.addElement(measurement.getP2());
    }

    public void updateAndCheckForSnapshot(long timestamp) {
//        System.out.println(timestamp + ";" +  nextSnapshotTime);
        while (timestamp >= nextSnapshotTime) {
            long watermark = nextSnapshotTime;
            calculateAddSnapshot(watermark);
            nextSnapshotTime = TimeUtil.add5MinuteFromNs(watermark);
        }
    }

    public void calculateAddSnapshot(long watermark) {
        removeMeasurementsWithWatermark(watermark);
        updateAqi(watermark);
        addSnapshot(watermark);
    }

    public void checkForSnapshot(long timestamp) {
        if (timestamp >= nextSnapshotTime) {
            long watermark = nextSnapshotTime;
            addSnapshot(watermark);
            nextSnapshotTime = TimeUtil.getNext5MinuteFromNs(watermark);
        }
    }

    public void addSnapshot(long watermark) {
        if (activeAQI != null) {
            snapshots.add(new AirQualityIndex(watermark, activeAQI));
            snapshotsAveragePair.addElement(activeAQI);
        }
    }

    private void updateAverageAqi(long watermark) {
        OptionalDouble avgAqiOpt = snapshotsAveragePair.getCount() == 0 ? OptionalDouble.empty() :
                OptionalDouble.of(snapshotsAveragePair.getSum() / snapshotsAveragePair.getCount());

        averageAQI = avgAqiOpt.isPresent() ? (int) Math.round(avgAqiOpt.getAsDouble()) : null;
    }

    public boolean isActive(long lastTimestamp, long watermark) {
        return lastTimestamp > (watermark - 10 * TimeUtil.MINUTE_PERIOD_NS);
    }

    public PriorityQueue<SimpleMeasurement> removeMeasurementsWithWatermark(long watermark) {
        SimpleMeasurement head = measurements.peek();
        while (head != null && head.getTimestamp() < watermark - measurementsLowerBoundDiminutive) {
            SimpleMeasurement measurement = measurements.remove();
            p1AveragePair.removeElement(measurement.getP1());
            p2AveragePair.removeElement(measurement.getP2());
            head = measurements.peek();
        }
        return measurements;
    }

    public PriorityQueue<AirQualityIndex> removeSnapshotsWithWatermark(long watermark) {
        AirQualityIndex head = snapshots.peek();
        while (head != null && head.getTimestamp() < watermark - snapshotsLowerBoundDiminutive) {
            AirQualityIndex index = snapshots.remove();//remove(head)
            snapshotsAveragePair.removeElement(index.getAqi());
            head = snapshots.peek();
        }
        return snapshots;
    }

    public void updateAqi(long watermark) {
        currentAqiP1 = null;
        currentAqiP2 = null;
        Double avgP1 = p1AveragePair.getCount() == 0 ? null :
                (p1AveragePair.getSum() / p1AveragePair.getCount());
        Double avgP2 = p2AveragePair.getCount() == 0 ? null :
                (p2AveragePair.getSum() / p2AveragePair.getCount());

        AQICalculator calculator = AQICalculator.getAQICalculatorInstance();
        if (avgP1 != null) {
            currentAqiP1 = calculator.getAQI(Pollutant.PM10, avgP1);
        }
        if (avgP2 != null) {
            currentAqiP2 = calculator.getAQI(Pollutant.PM25, avgP2);
        }

        if (currentAqiP1 != null && currentAqiP1 == -1) {
            currentAqiP1 = null;
        }
        if (currentAqiP2 != null && currentAqiP2 == -1) {
            currentAqiP2 = null;
        }

        if (currentAqiP1 == null || currentAqiP2 == null) {
            if (currentAqiP1 == null) activeAQI = currentAqiP2;
            else activeAQI = currentAqiP1;
        } else activeAQI = Math.max(currentAqiP1, currentAqiP2);
//        currentAqiP1 = (currentAqiP1 == null) ? -1 : currentAqiP1;
//        currentAqiP2 = (currentAqiP2 == null) ? -1 : currentAqiP2;

        if (currentMeasurements) {
            processGoodAqi(activeAQI, watermark);
        }
    }

    private void processGoodAqi(Integer activeAQI, long watermark) {
        if (activeAQI == null) {
            goodSinceTimestamp = null;
            return;
        } else if (activeAQI <= GOOD_AQI_UPPER_BOUND) {//AQI is good
            if (goodSinceTimestamp == null) {
                goodSinceTimestamp = watermark;
            }
        } else {//AQI is not good
            if (goodSinceTimestamp != null) {
                goodSinceTimestamp = null;
            }
        }
    }

    public PriorityQueue<SimpleMeasurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(PriorityQueue<SimpleMeasurement> measurements) {
        this.measurements = measurements;
    }

    public PriorityQueue<AirQualityIndex> getSnapshots() {
        return snapshots;
    }

    public void setSnapshots(PriorityQueue<AirQualityIndex> snapshots) {
        this.snapshots = snapshots;
    }

    public Integer getCurrentAqiP1() {
        return currentAqiP1;
    }

    public void setCurrentAqiP1(Integer currentAqiP1) {
        this.currentAqiP1 = currentAqiP1;
    }

    public Integer getCurrentAqiP2() {
        return currentAqiP2;
    }

    public void setCurrentAqiP2(Integer currentAqiP2) {
        this.currentAqiP2 = currentAqiP2;
    }

    public Integer getActiveAQI() {
        return activeAQI;
    }

    public void setActiveAQI(Integer activeAQI) {
        this.activeAQI = activeAQI;
    }

    public Integer getAverageAQI() {
        return averageAQI;
    }

    public void setAverageAQI(Integer averageAQI) {
        this.averageAQI = averageAQI;
    }

    public AveragePair getP1AveragePair() {
        return p1AveragePair;
    }

    public void setP1AveragePair(AveragePair p1AveragePair) {
        this.p1AveragePair = p1AveragePair;
    }

    public AveragePair getP2AveragePair() {
        return p2AveragePair;
    }

    public void setP2AveragePair(AveragePair p2AveragePair) {
        this.p2AveragePair = p2AveragePair;
    }

    public AveragePair getSnapshotsAveragePair() {
        return snapshotsAveragePair;
    }

    public void setSnapshotsAveragePair(AveragePair snapshotsAveragePair) {
        this.snapshotsAveragePair = snapshotsAveragePair;
    }

    public Long getLastTimestamp() {
        return lastTimestamp;
    }

    public void setLastTimestamp(Long lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }

    public Long getNextSnapshotTime() {
        return nextSnapshotTime;
    }

    public void setNextSnapshotTime(Long nextSnapshotTime) {
        this.nextSnapshotTime = nextSnapshotTime;
    }

    public Long getGoodSinceTimestamp() {
        return goodSinceTimestamp;
    }

    public void setGoodSinceTimestamp(Long goodSinceTimestamp) {
        this.goodSinceTimestamp = goodSinceTimestamp;
    }

    public long getMeasurementsLowerBoundDiminutive() {
        return measurementsLowerBoundDiminutive;
    }

    public void setMeasurementsLowerBoundDiminutive(long measurementsLowerBoundDiminutive) {
        this.measurementsLowerBoundDiminutive = measurementsLowerBoundDiminutive;
    }

    public long getSnapshotsLowerBoundDiminutive() {
        return snapshotsLowerBoundDiminutive;
    }

    public void setSnapshotsLowerBoundDiminutive(long snapshotsLowerBoundDiminutive) {
        this.snapshotsLowerBoundDiminutive = snapshotsLowerBoundDiminutive;
    }

    public boolean isCurrentMeasurements() {
        return currentMeasurements;
    }

    public void setCurrentMeasurements(boolean currentMeasurements) {
        this.currentMeasurements = currentMeasurements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AqiState aqiState = (AqiState) o;
        return measurementsLowerBoundDiminutive == aqiState.measurementsLowerBoundDiminutive &&
                snapshotsLowerBoundDiminutive == aqiState.snapshotsLowerBoundDiminutive &&
                currentMeasurements == aqiState.currentMeasurements &&
                Objects.equals(measurements, aqiState.measurements) &&
                Objects.equals(snapshots, aqiState.snapshots) &&
                Objects.equals(currentAqiP1, aqiState.currentAqiP1) &&
                Objects.equals(currentAqiP2, aqiState.currentAqiP2) &&
                Objects.equals(activeAQI, aqiState.activeAQI) &&
                Objects.equals(averageAQI, aqiState.averageAQI) &&
                Objects.equals(p1AveragePair, aqiState.p1AveragePair) &&
                Objects.equals(p2AveragePair, aqiState.p2AveragePair) &&
                Objects.equals(snapshotsAveragePair, aqiState.snapshotsAveragePair) &&
                Objects.equals(lastTimestamp, aqiState.lastTimestamp) &&
                Objects.equals(nextSnapshotTime, aqiState.nextSnapshotTime) &&
                Objects.equals(goodSinceTimestamp, aqiState.goodSinceTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(measurements, snapshots, currentAqiP1, currentAqiP2, activeAQI, averageAQI, p1AveragePair, p2AveragePair, snapshotsAveragePair, lastTimestamp, nextSnapshotTime, goodSinceTimestamp, measurementsLowerBoundDiminutive, snapshotsLowerBoundDiminutive, currentMeasurements);
    }
}
