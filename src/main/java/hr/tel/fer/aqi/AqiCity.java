package hr.tel.fer.aqi;

import com.thanglequoc.aqicalculator.AQICalculator;
import com.thanglequoc.aqicalculator.Pollutant;

import java.io.Serializable;
import java.util.Objects;

public class AqiCity implements Serializable {
    private String city;
    private Integer aqi;
    private Integer aqiP1;
    private Integer aqiP2;

    public static AqiCity of(String city, Integer aqi, Integer aqiP1, Integer aqiP2){
        return new AqiCity(city,aqi,aqiP1,aqiP2);
    }

    public AqiCity(){

    }

    public AqiCity(String city, Integer aqi, Integer aqiP1, Integer aqiP2) {
        this.city = city;
        this.aqi = aqi;
        this.aqiP1 = aqiP1;
        this.aqiP2 = aqiP2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getAqi() {
        return aqi;
    }

    public void setAqi(Integer aqi) {
        this.aqi = aqi;
    }

    public Integer getAqiP1() {
        return aqiP1;
    }

    public void setAqiP1(Integer aqiP1) {
        this.aqiP1 = aqiP1;
    }

    public Integer getAqiP2() {
        return aqiP2;
    }

    public void setAqiP2(Integer aqiP2) {
        this.aqiP2 = aqiP2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AqiCity aqiCity = (AqiCity) o;
        return aqi == aqiCity.aqi &&
                aqiP1 == aqiCity.aqiP1 &&
                aqiP2 == aqiCity.aqiP2 &&
                Objects.equals(city, aqiCity.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, aqi, aqiP1, aqiP2);
    }

    @Override
    public String toString() {
        return "AqiCity{" +
                "city='" + city + '\'' +
                ", aqi=" + aqi +
                ", aqiP1=" + aqiP1 +
                ", aqiP2=" + aqiP2 +
                '}';
    }

    public static void main(String[] args){
        int aqi = AQICalculator.getAQICalculatorInstance().getAQI(Pollutant.PM25, 680);
        System.out.println(aqi);
        System.out.println(Long.MAX_VALUE);
    }
}
