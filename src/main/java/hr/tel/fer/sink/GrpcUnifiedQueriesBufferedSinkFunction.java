package hr.tel.fer.sink;

import com.google.common.primitives.Longs;
import de.tum.i13.proto.*;
import hr.tel.fer.aqi.QueryResponse;
import hr.tel.fer.factories.StubManager;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class GrpcUnifiedQueriesBufferedSinkFunction extends RichSinkFunction<QueryResponse> {

    private PriorityQueue<QueryResponse> buffered;

    private int lastSentIndex;

    private Benchmark benchmark;

    public GrpcUnifiedQueriesBufferedSinkFunction(Benchmark benchmark, String address){
        this.benchmark = benchmark;
        this.buffered = new PriorityQueue<QueryResponse>(10, new ComparatorImpl());
        this.lastSentIndex = -1;
    }

    public class ComparatorImpl implements Serializable, Comparator<QueryResponse>{

        @Override
        public int compare(QueryResponse t1, QueryResponse t2){
            return Longs.compare(t1.getBatchId(), t2.getBatchId());
        }

    }

    @Override
    public void invoke(QueryResponse value, Context context) throws Exception {
        long index = value.getBatchId();
        if(index == lastSentIndex+1){
            sendBuffered(context);
            sendTuple(value, context);
        }else{
            buffered.add(value);
        }
//        if(value.f0.size()==0 || value.f1%1000==0 || value.f1==29999) {
//            System.out.println("[" + value.f1 + "]Q1 out:" + q1Result.getTopkimprovedList());
//        }
    }

    public void sendBuffered(Context context){
        QueryResponse head = buffered.peek();
        while(head != null && head.getBatchId() == lastSentIndex+1){
            sendTuple(head, context);
            buffered.remove(head);
            head = buffered.peek();
        }
    }

    private void sendTuple(QueryResponse value, Context context){
        AtomicInteger index = new AtomicInteger();
        ResultQ1 q1Result = ResultQ1.newBuilder()
                .setBenchmarkId(benchmark.getId())
                .setBatchSeqId(value.getBatchId()) //set the sequence number
                .addAllTopkimproved(value.getTopKCities().stream().map(aqiCity ->
                        new TopKCities().toBuilder()
                                .setPosition(index.incrementAndGet())
                                .setCity(aqiCity.getCity())
                                .setAverageAQIImprovement(aqiCity.getAqi())
                                .setCurrentAQIP1(aqiCity.getAqiP1())
                                .setCurrentAQIP2(aqiCity.getAqiP2()).build()).collect(Collectors.toList()))
                .build();

        StubManager.resultQ1(q1Result);

        ResultQ2 q2Result = ResultQ2.newBuilder()
                .setBenchmarkId(benchmark.getId())
                .setBatchSeqId(value.getBatchId()) //set the sequence number
                .addAllHistogram(value.getTopKStreaks().stream().map(streak ->
                        new TopKStreaks().toBuilder()
                                .setBucketFrom(streak.getBucketFrom())
                                .setBucketTo(streak.getBucketTo())
                                .setBucketPercent(streak.getActiveCitiesPercentage())
                                .build()).collect(Collectors.toList()))
                .build();

        StubManager.resultQ2(q2Result);

        if(value.getBatchId()%1000==0){
            System.out.println("[" + value.getTopKCities() + "]Q1 out:" + q1Result.getTopkimprovedList());
            System.out.println("[" + value.getTopKStreaks() + "]Q2 out:" + q2Result.getHistogramList());
        }

        lastSentIndex++;

    }

    public static void write(Object obj, String path){
        try {
            FileWriter myWriter = new FileWriter(path);

            try{myWriter.write(obj+"\n");}
            catch(IOException ex){
                throw new RuntimeException(ex);
            }

            myWriter.close();
//            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
