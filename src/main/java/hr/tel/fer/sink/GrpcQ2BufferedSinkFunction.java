package hr.tel.fer.sink;

import de.tum.i13.proto.Benchmark;
import de.tum.i13.proto.ResultQ1;
import de.tum.i13.proto.TopKCities;
import hr.tel.fer.aqi.AqiCity;
import hr.tel.fer.factories.StubManager;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class GrpcQ2BufferedSinkFunction extends RichSinkFunction<Tuple2<List<AqiCity>, Long>> {

    private PriorityQueue<Tuple2<List<AqiCity>, Long>> buffered;

    private int lastSentIndex;

    private Benchmark benchmark;

    public GrpcQ2BufferedSinkFunction(Benchmark benchmark, String address){
        this.benchmark = benchmark;
        this.buffered = new PriorityQueue<Tuple2<List<AqiCity>, Long>>(10, Comparator.comparing(tup -> tup.f1));
        this.lastSentIndex = -1;
    }

    @Override
    public void invoke(Tuple2<List<AqiCity>, Long> value, Context context) throws Exception {
        long index = value.f1;
        if(index == lastSentIndex+1){
            sendBuffered(context);
            sendTuple(value, context);
        }else{
            buffered.add(value);
        }
//        if(value.f0.size()==0 || value.f1%1000==0 || value.f1==29999) {
//            System.out.println("[" + value.f1 + "]Q1 out:" + q1Result.getTopkimprovedList());
//        }
    }

    public void sendBuffered(Context context){
        Tuple2<List<AqiCity>,Long> head = buffered.peek();
        while(head != null && head.f1 == lastSentIndex+1){
            sendTuple(head, context);
            buffered.remove(head);
            head = buffered.peek();
        }
    }

    private void sendTuple(Tuple2<List<AqiCity>, Long> value, Context context){
        AtomicInteger index = new AtomicInteger();
        ResultQ1 q1Result = ResultQ1.newBuilder()
                .setBenchmarkId(benchmark.getId())
                .setBatchSeqId(value.f1) //set the sequence number
                .addAllTopkimproved(value.f0.stream().map(aqiCity ->
                        new TopKCities().toBuilder()
                                .setPosition(index.incrementAndGet())
                                .setCity(aqiCity.getCity())
                                .setAverageAQIImprovement(aqiCity.getAqi())
                                .setCurrentAQIP1(aqiCity.getAqiP1())
                                .setCurrentAQIP2(aqiCity.getAqiP2()).build()).collect(Collectors.toList()))
                .build();

        StubManager.resultQ1(q1Result);
        lastSentIndex++;

    }

    public static void write(Object obj, String path){
        try {
            FileWriter myWriter = new FileWriter(path);

            try{myWriter.write(obj+"\n");}
            catch(IOException ex){
                throw new RuntimeException(ex);
            }

            myWriter.close();
//            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
