package hr.tel.fer.sink;

import de.tum.i13.proto.Benchmark;
import de.tum.i13.proto.ResultQ1;
import de.tum.i13.proto.TopKCities;
import hr.tel.fer.aqi.AqiCity;
import hr.tel.fer.factories.StubManager;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class GrpcQ1SinkFunction extends RichSinkFunction<Tuple2<List<AqiCity>, Long>> {

    private Benchmark benchmark;

    public GrpcQ1SinkFunction(Benchmark benchmark){
        this.benchmark = benchmark;
    }

    @Override
    public void invoke(Tuple2<List<AqiCity>, Long> value, Context context) throws Exception {

        AtomicInteger index = new AtomicInteger();
        ResultQ1 q1Result = ResultQ1.newBuilder()
                .setBenchmarkId(benchmark.getId())
                .setBatchSeqId(value.f1) //set the sequence number
                .addAllTopkimproved(value.f0.stream().map(aqiCity ->
                        new TopKCities().toBuilder()
                                .setPosition(index.incrementAndGet())
                                .setCity(aqiCity.getCity())
                                .setAverageAQIImprovement(aqiCity.getAqi())
                                .setCurrentAQIP1(aqiCity.getAqiP1())
                                .setCurrentAQIP2(aqiCity.getAqiP2()).build()).collect(Collectors.toList()))
                .build();

        StubManager.resultQ1(q1Result);

//        if(value.f0.size()==0 || value.f1%1000==0 || value.f1==29999) {
//            System.out.println("[" + value.f1 + "]Q1 out:" + q1Result.getTopkimprovedList());
//        }
    }

    public static void write(Object obj, String path){
        try {
            FileWriter myWriter = new FileWriter(path);

            try{myWriter.write(obj+"\n");}
            catch(IOException ex){
                throw new RuntimeException(ex);
            }

            myWriter.close();
//            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
