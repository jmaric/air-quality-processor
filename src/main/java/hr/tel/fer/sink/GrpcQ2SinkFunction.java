package hr.tel.fer.sink;

import de.tum.i13.proto.Benchmark;
import de.tum.i13.proto.ResultQ2;
import de.tum.i13.proto.TopKStreaks;
import hr.tel.fer.factories.StubManager;
import hr.tel.fer.streak.GoodAqiStreak;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class GrpcQ2SinkFunction extends RichSinkFunction<Tuple2<List<GoodAqiStreak>, Long>> {

     private Benchmark benchmark;

    public GrpcQ2SinkFunction(Benchmark benchmark){
        this.benchmark = benchmark;
    }

    @Override
    public void invoke(Tuple2<List<GoodAqiStreak>, Long> value, Context context) throws Exception {

        ResultQ2 q2Result = ResultQ2.newBuilder()
                .setBenchmarkId(benchmark.getId())
                .setBatchSeqId(value.f1) //set the sequence number
                .addAllHistogram(value.f0.stream().map(streak ->
                        new TopKStreaks().toBuilder()
                                .setBucketFrom(streak.getBucketFrom())
                                .setBucketTo(streak.getBucketTo())
                                .setBucketPercent(streak.getActiveCitiesPercentage())
                                .build()).collect(Collectors.toList()))
                .build();

//        q1Result.getTopkimprovedList().forEach(t -> t.toBuilder().build().writeTo());

        StubManager.resultQ2(q2Result);

//        if(value.f1%1000==0){
//            System.out.println("[" + value.f1 + "]Q2 out:" + q2Result.getHistogramList());
//        }
    }

    public static void write(Object obj, String path){
        try {
            FileWriter myWriter = new FileWriter(path);

            try{myWriter.write(obj.toString()+"\n");}
            catch(IOException ex){
                throw new RuntimeException(ex);
            }

            myWriter.close();
//            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
