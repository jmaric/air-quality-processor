package hr.tel.fer.polygon;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;

import java.io.Serializable;

public class PolygonTransformer implements Serializable {
    private GeometryFactory geometryFactory;
    public PolygonTransformer(){
        geometryFactory = new GeometryFactory();
    }
    public Polygon transformPolyFromProtoToJts(de.tum.i13.proto.Polygon polygon){
        return geometryFactory.createPolygon(polygon.getPointsList().stream()
                .map(point -> new Coordinate(point.getLongitude(), point.getLatitude())).toArray(Coordinate[]::new));
    }
}
