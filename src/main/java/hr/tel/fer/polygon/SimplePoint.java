package hr.tel.fer.polygon;

import java.io.Serializable;
import java.util.Objects;

public class SimplePoint implements Serializable {
    private float longitude;
    private float lattitude;

    public SimplePoint(){

    }

    public SimplePoint(float longitude, float lattitude) {
        this.longitude = longitude;
        this.lattitude = lattitude;
    }

    public static SimplePoint of(float longitude, float lattitude){
        return new SimplePoint(longitude, lattitude);
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLattitude() {
        return lattitude;
    }

    public void setLattitude(float lattitude) {
        this.lattitude = lattitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimplePoint that = (SimplePoint) o;
        return
                Math.abs(that.longitude - longitude) < 0.01 &&
                Math.abs(that.lattitude - lattitude) < 0.01;
//                Float.compare(that.longitude, longitude) == 0 &&
//                Float.compare(that.lattitude, lattitude) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(longitude, lattitude);
    }
}
