package hr.tel.fer.polygon;

import org.locationtech.jts.geom.Polygon;

import java.io.Serializable;
import java.util.Objects;

public class PolygonCity implements Serializable {
    private Polygon polgyon;
    private String city;

    public PolygonCity(Polygon polgyon, String city) {
        this.polgyon = polgyon;
        this.city = city;
    }

    public Polygon getPolgyon() {
        return polgyon;
    }

    public void setPolgyon(Polygon polgyon) {
        this.polgyon = polgyon;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PolygonCity that = (PolygonCity) o;
        return Objects.equals(polgyon, that.polgyon) &&
                Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(polgyon, city);
    }
}
