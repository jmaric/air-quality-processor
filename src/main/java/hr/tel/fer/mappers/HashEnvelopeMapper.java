package hr.tel.fer.mappers;

import hr.tel.fer.measurement.InputMeasurement;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.GeometryFactory;

import java.util.Objects;

public class HashEnvelopeMapper implements FlatMapFunction<InputMeasurement, Tuple2<InputMeasurement, Integer>> {

    private Envelope envelope;
    private int parallelism;

    public HashEnvelopeMapper(Envelope envelope, int parallelism){
        this.envelope = envelope;
        this.parallelism = parallelism;
    }

    private Coordinate getPoint(double lon, double lat){
        Coordinate coord = new Coordinate(lon, lat);
        return coord;
    }

    @Override
    public void flatMap(InputMeasurement inputMeasurement, Collector<Tuple2<InputMeasurement, Integer>> collector) throws Exception {
        if(envelope.contains(getPoint(inputMeasurement.getLongitude(), inputMeasurement.getLatitude()))){
            collector.collect(Tuple2.of(inputMeasurement, Objects.hash(roundAvoid(inputMeasurement.getLongitude(),1),
                    roundAvoid(inputMeasurement.getLatitude(),1))%parallelism));
        }
    }

    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    public static void main(String[] args){
        System.out.println(Objects.hash(roundAvoid(47.792,1), roundAvoid(9.628,1)));
        System.out.println(Objects.hash(roundAvoid(47.798,1), roundAvoid(9.632,1)));
    }
}
