package hr.tel.fer.mappers;

import hr.tel.fer.measurement.InputMeasurement;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.GeometryFactory;

import java.util.List;

public class EnvelopeMapper implements FlatMapFunction<InputMeasurement, Tuple2<InputMeasurement, Integer>> {

    private List<Envelope> envelopes;

    public EnvelopeMapper(List<Envelope> envelopes){
        this.envelopes = envelopes;
    }

    private Coordinate getPoint(double lon, double lat){
        Coordinate coord = new Coordinate(lon, lat);
        return coord;
    }

    @Override
    public void flatMap(InputMeasurement inputMeasurement, Collector<Tuple2<InputMeasurement, Integer>> collector) throws Exception {
        for(int i = 0; i < envelopes.size(); i++){
            if(envelopes.get(i).contains(getPoint(inputMeasurement.getLongitude(), inputMeasurement.getLatitude()))){
                collector.collect(Tuple2.of(inputMeasurement, i));
            }
        }
    }
}
