package hr.tel.fer.settings;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.OutputTag;

public class ProjectSettings {

    public static final java.lang.String DEFAULT_PROCESS_PARALLELISM_NAME = "process_parallelism";
    public static final int DEFAULT_PROCESS_PARALLELISM = 4;

    public static final java.lang.String MAX_BATCHES_NAME = "max_batches";
    public static final int DEFAULT_MAX_BATCHES = 30000;

    public static final java.lang.String CITY_FINDER_PARALLELIZATION_NAME = "city_finder_parallelism";
    public static final int DEFAULT_CITY_FINDER_PARALLELIZATION = 3;

    public static final java.lang.String SOURCE_PARALLELIZATION_NAME = "source_parallelism";
    public static final int DEFAULT_SOURCE_PARALLELIZATION = 4;

    public static final java.lang.String SOURCE_SLEEP_MS_NAME = "source_sleep_ms";
    public static final int DEFAULT_SOURCE_SLEEP_MS = -1;

    public static final java.lang.String USE_SPATIAL_INDEX_NAME = "use_spatial_index";
    public static final boolean DEFAULT_USE_SPATIAL_INDEX = true;

    public static final java.lang.String EXECUTION_TYPE_NAME = "execution_type";
    public static final String DEFAULT_EXECUTION_TYPE = "test";

    public static final String INTERNAL_CHANNEL_ADDRESS = "192.168.1.4";
    public static final String EXTERNAL_CHANNEL_ADDRESS = "challenge.msrg.in.tum.de";

    public static final String GOOD_AIR_QUALITY_OUTPUT_TAG = "good-air-quality-tag";

    public static final OutputTag<Tuple2<Integer, Long>> goodAirQualityOutputTag = new OutputTag<Tuple2<Integer, Long>>(GOOD_AIR_QUALITY_OUTPUT_TAG) {
    };


//    public static final OutputTag<Tuple2<MeasurementCity, Long>> lastYearMeasurementsOutputTag = new OutputTag<Tuple2<MeasurementCity, Long>>(LAST_YEAR_MEASUREMENTS_TAG) {
//    };

//    public static final OutputTag<Tuple2<SipMatchPair, String>> outputTag = new OutputTag<Tuple2<SipMatchPair, String>>(CALL_ID_FROM_MAP_TAG) {
//    };
//
//    public static final OutputTag<Tuple2<IProtocolMessage, Boolean>> multipliedTag = new OutputTag<Tuple2<IProtocolMessage, Boolean>>(MULTIPLIED_MESSAGES_TAG) {
//    };

}
