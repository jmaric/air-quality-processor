package hr.tel.fer;

import de.tum.i13.proto.Benchmark;
import de.tum.i13.proto.Location;
import hr.tel.fer.aqi.AqiCity;
import hr.tel.fer.city.CityFinder;
import hr.tel.fer.city.CityFinderIndex;
import hr.tel.fer.city.CityFinderSeq;
import hr.tel.fer.factories.StubManager;
import hr.tel.fer.mappers.HashEnvelopeMapper;
import hr.tel.fer.processes.MeasurementCityKeyedProcess;
import hr.tel.fer.measurement.InputMeasurement;
import hr.tel.fer.measurement.MeasurementCity;
import hr.tel.fer.processes.*;
import hr.tel.fer.processes.watermark.TimestampsAndWatermarksAssigner;
import hr.tel.fer.settings.ProjectSettings;
import hr.tel.fer.sink.GrpcQ1SinkFunction;
import hr.tel.fer.sink.GrpcQ2SinkFunction;
import hr.tel.fer.source.GrpcSourceFunctionInputMeasurementRich;
import hr.tel.fer.streak.GoodAqiStreak;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static hr.tel.fer.time.TimeUtil.DAY_PERIOD_NS;

public class AQP {

    private static final Logger LOG = LoggerFactory.getLogger(AQP.class);

    public static void main(String[] args) throws Exception {

        printUsage();

        final ParameterTool parameterTool = ParameterTool.fromArgs(args);

        StreamExecutionEnvironment env;
        env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.getConfig().setAutoWatermarkInterval(0);

        int processParallelization = parameterTool.getInt(ProjectSettings.DEFAULT_PROCESS_PARALLELISM_NAME,
                ProjectSettings.DEFAULT_PROCESS_PARALLELISM);

        env.setParallelism(processParallelization);

        int maxBatches = parameterTool.getInt(ProjectSettings.MAX_BATCHES_NAME,
                ProjectSettings.DEFAULT_MAX_BATCHES);

        int cityFinderParallelization = parameterTool.getInt(ProjectSettings.CITY_FINDER_PARALLELIZATION_NAME,
                processParallelization);

        int sourceParallelization = parameterTool.getInt(ProjectSettings.SOURCE_PARALLELIZATION_NAME,
                processParallelization);

        boolean useSpatialIndex = parameterTool.getBoolean(ProjectSettings.USE_SPATIAL_INDEX_NAME,
                ProjectSettings.DEFAULT_USE_SPATIAL_INDEX);

        int sourceSleepMs = parameterTool.getInt(ProjectSettings.SOURCE_SLEEP_MS_NAME,
                ProjectSettings.DEFAULT_SOURCE_SLEEP_MS);

        String type = parameterTool.get(ProjectSettings.EXECUTION_TYPE_NAME, ProjectSettings.DEFAULT_EXECUTION_TYPE);

        Benchmark benchmark = StubManager.createGCBenchmark(type);
        List<Location> locations = StubManager.getLocations(benchmark);
        CityFinder cityFinder = useSpatialIndex ? new CityFinderIndex(locations) : new CityFinderSeq(locations);

        DataStream<InputMeasurement> inputMeasurements = env.addSource(
                new GrpcSourceFunctionInputMeasurementRich(benchmark, maxBatches, sourceSleepMs), "Input Batches").setParallelism(sourceParallelization);

        SingleOutputStreamOperator<Tuple3<Long, Long, Integer>> batchWatermarks = inputMeasurements.filter(im -> im.getBatchLast()).setParallelism(sourceParallelization)
                .name("Last-Per-Batch Measurements")
                .keyBy(im -> "").process(new BatchIdExtractor()).setParallelism(1)
                .returns(TypeInformation.of(new TypeHint<Tuple3<Long, Long, Integer>>() {})).name("Batch ID Extractor");

        DataStream<Tuple4<MeasurementCity, Boolean, Long, Long>> measurementCities = inputMeasurements
                .flatMap(new HashEnvelopeMapper(cityFinder.getEnvelope(), cityFinderParallelization*10)).setParallelism(sourceParallelization)
                .keyBy(value -> value.f1)
                .process(new MeasurementCityKeyedProcess(cityFinder)).setParallelism(cityFinderParallelization)
                .name("City Locator");

        measurementCities = TimestampsAndWatermarksAssigner.assignTimestampsMeasurements(measurementCities)
                .setParallelism(cityFinderParallelization);

        SingleOutputStreamOperator<Tuple3<AqiCity, AqiCity, Long>> cityResponses = measurementCities.keyBy(tup -> tup.f0.getCity())
                .process(new AQIProcessorUnified(DAY_PERIOD_NS, 5* DAY_PERIOD_NS));

        DataStream<Tuple2<List<AqiCity>, Long>> batchTopKCities = cityResponses.keyBy(tup3 -> tup3.f2)
                .connect(batchWatermarks.keyBy(tup -> tup.f1)).process(new Query1ResponserUnified()).name("Query1 Responser");

        DataStream<Tuple2<List<GoodAqiStreak>, Long>> batchTopKStreaks = cityResponses.getSideOutput(ProjectSettings.goodAirQualityOutputTag)
                .keyBy(tup -> tup.f1).connect(batchWatermarks.keyBy(tup -> tup.f1)).process(new Query2Responser()).name("Query2 Responser");

        batchTopKCities.addSink(new GrpcQ1SinkFunction(benchmark)).name("GrpcQ1 Sink");

        batchTopKStreaks.addSink(new GrpcQ2SinkFunction(benchmark)).name("GrpcQ2 Sink");
        env.execute("Air Quality Processor");
    }

    private static void printUsage() {
        LOG.info("Executing AQP with default props.");
        LOG.info("Usage: flink run <jar name>.jar " +
                "--process_parallelism <processing parallelism (default is 4) - not bigger than number of tasks slots> " +
                "--city_finder_parallelism <int (default is process_parallelism)>" +
                "--source_parallelism <int (default is process_parallelism)>" +
                "--time_to_live <long (default is 60000ms (one minute))>" +
                "--max_batches <long>" +
                "--use_spatial_index <boolean (default is true)>" +
                "--source_sleep_ms <int (default is 0)>" +
                "--execution_type <string evaluation/test (default is test)>");
    }
}
