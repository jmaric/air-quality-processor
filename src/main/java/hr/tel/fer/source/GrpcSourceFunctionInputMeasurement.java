package hr.tel.fer.source;

import de.tum.i13.proto.Batch;
import de.tum.i13.proto.Benchmark;
import de.tum.i13.proto.Measurement;
import hr.tel.fer.factories.StubManager;
import hr.tel.fer.measurement.InputMeasurement;
import hr.tel.fer.time.TimeUtil;
import io.grpc.StatusRuntimeException;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class GrpcSourceFunctionInputMeasurement implements SourceFunction<InputMeasurement> {

    private static final Logger LOG = LoggerFactory.getLogger(GrpcSourceFunctionInputMeasurement.class);

    private volatile boolean run = true;
    private Long watermark;
    private Benchmark benchmark;
    private int maxBatches;
    private Integer threadSleepMs;

    public GrpcSourceFunctionInputMeasurement(Benchmark benchmark, int maxBatches, int threadSleepMs, String address){
        this.benchmark = benchmark;
        if(maxBatches == -1) maxBatches = Integer.MAX_VALUE;
        this.maxBatches = maxBatches;
        this.threadSleepMs = threadSleepMs > 0 ? threadSleepMs : null;
    }

    @Override
    public void run(SourceContext<InputMeasurement> ctx) throws Exception {
        StubManager.startBenchmark(benchmark);
        int batchesInCounter = 0;
        while(run) {
            Batch batch = null;
            try{
                batch = StubManager.nextBatch(benchmark);
            }catch(StatusRuntimeException ex){
                continue;
            }
            Long seqId = batch.getSeqId();
            List<Measurement> currentMeasurements = batch.getCurrentList();
            List<Measurement> lastYearMeasurements = batch.getLastyearList();
            if(currentMeasurements.size() == 0){
                LOG.info("[" + batch.getSeqId() + "]" + "0 current measurements.");
                watermark++;
                ctx.collect(InputMeasurement.watermarkCurrentMeasurement(watermark));//this is just so the watermark can keep on
            }else{
                watermark = TimeUtil.timestampToNanoSeconds(currentMeasurements.get(currentMeasurements.size()-1).getTimestamp());
            }
            if(lastYearMeasurements.size() == 0){//this is also just so watermark can be notified on changing
                ctx.collect(InputMeasurement.watermarkLyMeasurement(watermark));
            }
            Boolean batchLast = null;
            Long batchFirstTimestamp = null;
            if(seqId == 0) batchFirstTimestamp = TimeUtil.timestampToNanoSeconds(currentMeasurements.get(0).getTimestamp());
//            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("src/main/resources/batches/batch-"+batch.getSeqId() + "-" + watermark + ".txt", true)));
            for (int i = 0; i < currentMeasurements.size(); i++) {
                batchLast = (i == currentMeasurements.size()-1) ? true : false;
                Measurement current = currentMeasurements.get(i);
                long timestamp = TimeUtil.timestampToNanoSeconds(current.getTimestamp());
//                if(batchLast){
//                    watermark = timestamp;
//                }
                synchronized (ctx.getCheckpointLock()) {
                    InputMeasurement im = new InputMeasurement(timestamp,
                            current.getLatitude(),
                            current.getLongitude(),
                            current.getP1(),
                            current.getP2(),
                            true,
                            batchLast,
                            watermark,
                            seqId,
                            batchFirstTimestamp);
                    ctx.collect(im);
//                    System.out.println("Sent.");
                }
            }

            boolean batchLastInLastYears = false;
            if(batchLast == null){
                batchLastInLastYears = true;
            }
            batchLast = false;
            for (int i = 0; i < lastYearMeasurements.size(); i++) {
                if((i == lastYearMeasurements.size()-1) && batchLastInLastYears){
                    batchLast = true;
                }
                Measurement lastYearMeas = lastYearMeasurements.get(i);
                long timestamp = TimeUtil.timestampToNanoSeconds(lastYearMeas.getTimestamp());
                synchronized (ctx.getCheckpointLock()) {
                    ctx.collect(new InputMeasurement(timestamp,
                            lastYearMeas.getLatitude(),
                            lastYearMeas.getLongitude(),
                            lastYearMeas.getP1(),
                            lastYearMeas.getP2(),
                            false,
                            batchLast,
                            watermark,
                            seqId));
//                    System.out.println("Sent.");
                }
            }
//            pw.println(currentMeasurements);
//            pw.println(lastYearMeasurements);
//            pw.close();
//            if(batch.getSeqId()%100 == 0){
                System.out.println("[" + batch.getSeqId() + "]" + "Collected " + currentMeasurements.size() + " current measurements.");
//            }

            batchesInCounter++;
            if(batchesInCounter > maxBatches || batch.getLast()){
//                ctx.emitWatermark(new Watermark(Long.MAX_VALUE));
                ctx.collect(InputMeasurement.FINAL_MEASUREMENT_CURRENT);
//                ctx.collect(InputMeasurement.FINAL_MEASUREMENT_LAST_YEAR);
                cancel();
//                ctx.close();
            }
            if(threadSleepMs != null){
                Thread.sleep(threadSleepMs);
            }

        }
    }

    @Override
    public void cancel() {
        System.out.println("Canceling");
        run=false;
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        StubManager.endBenchmark(benchmark);
    }
}
