package hr.tel.fer.time;

import com.google.protobuf.Timestamp;

import java.util.Calendar;

public class TimeUtil {

    public static final long YEAR_PERIOD_NS = (long)(365*24*60*60*1E9);
    public static final long DAY_PERIOD_NS = (long)(24*60*60*1E9);
    public static final int DAY_PERIOD_SEC = 24*60*60;
    public static final long MINUTE_PERIOD_NS = (long)(60*1E9);

    public static long timestampToNanoSeconds(Timestamp timestamp){
        return addNanosToSeconds(timestamp.getSeconds(), timestamp.getNanos());
    }

    public static long getTimestampFromYearAgoToNanoSeconds(Timestamp timestamp){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp.getSeconds()*1000);
//        cal.add(Calendar.YEAR, -1);
        cal.add(Calendar.DAY_OF_MONTH, -365);
        return addNanosToSeconds(cal.getTimeInMillis()/1000, timestamp.getNanos());
    }

    public static long addNanosToSeconds(long seconds, long nanos){
        return (long)(seconds*1E9) + nanos;
    }

    public static long getNext5MinuteFromNs(long timestamp){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis((long)(timestamp/1E9/60)*1000*60);
        int nextMinute = (cal.get(Calendar.MINUTE)/5+1)*5;
        cal.set(Calendar.MINUTE, nextMinute);
        return (long)(cal.getTimeInMillis()*1E6);
    }

    public static long add5MinuteFromNs(long timestamp){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis((long)(timestamp/1E6));
        cal.add(Calendar.MINUTE, 5);
        return (long)(cal.getTimeInMillis()*1E6);
    }

    public static void main(String[] args){
        System.out.println(add5MinuteFromNs(1614603900000000000L));
        System.out.println(add5MinuteFromNs(1614604200000000000L));
        System.out.println(getNext5MinuteFromNs(1614604200000000000L));
    }
}

