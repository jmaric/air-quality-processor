/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.tel.fer.spatial_index;

 

import java.util.List;

import de.tum.i13.proto.Polygon;
import hr.tel.fer.polygon.PolygonCity;
import org.locationtech.jts.index.SpatialIndex;
import org.locationtech.jts.index.hprtree.HPRtree;
import org.locationtech.jts.index.quadtree.Quadtree;
import org.locationtech.jts.index.strtree.STRtree;


public class SpatialIndexFactoryClean {

    public enum IndexType {
        STR_TREE, QUAD_TREE, HPR_TREE
    };

    protected static SpatialIndex create(IndexType indexType) {
        if (indexType == IndexType.QUAD_TREE) {
            return new Quadtree();
        } else if (indexType == IndexType.STR_TREE) {
            return new STRtree();
        } else {
            return new HPRtreeSerializable();
        }
    }

    public static SpatialIndex create(IndexType indexType, List<PolygonCity> polygonCities) {
        SpatialIndex index;
        if (indexType == IndexType.QUAD_TREE) {
            index = new Quadtree();
        } else if (indexType == IndexType.STR_TREE) {
            index = new STRtree();
        } else {
            index = new HPRtreeSerializable();
        }

        //add polygons to index
        polygonCities.forEach(polygonCity
                -> index.insert(polygonCity.getPolgyon().getEnvelopeInternal(), polygonCity));

        //build spatial index in the case of STRtree and HPRtree
        if (index instanceof STRtree) {
            ((STRtree) index).build();
        } else if (index instanceof HPRtreeSerializable) {
            ((HPRtreeSerializable) index).build();
        }

        return index;
    }
        
}