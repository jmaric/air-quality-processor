package hr.tel.fer.measurement;

import hr.tel.fer.time.TimeUtil;

import java.io.Serializable;
import java.util.Objects;

public class InputMeasurement implements Serializable, Comparable<InputMeasurement> {

    private static final long serialVersionUID = 1L;

    public static final InputMeasurement FINAL_MEASUREMENT_CURRENT = new InputMeasurement(Long.MAX_VALUE-1,89F, 179F , 100001F, 100001F, true, true, Long.MAX_VALUE, Long.MAX_VALUE);

//    public static final InputMeasurement FINAL_MEASUREMENT_LAST_YEAR = new InputMeasurement(Long.MAX_VALUE-TimeUtil.YEAR_PERIOD_NS-1,89F, 179F , 100001F, 100001F, false, true, Long.MAX_VALUE-TimeUtil.YEAR_PERIOD_NS, Long.MAX_VALUE);

    public static InputMeasurement watermarkLastSignal(Long watermark, Long batchSeqId){
        return new InputMeasurement(watermark,  89F, 179F ,100001F, 100001F, true, true, watermark, Long.MAX_VALUE);
    }

    public static InputMeasurement watermarkCurrentMeasurement(Long watermark){
        return new InputMeasurement(watermark,  89F, 179F ,100001F, 100001F, true, true, watermark, Long.MAX_VALUE);
    }

    public static InputMeasurement watermarkLyMeasurement(Long watermark){
        return new InputMeasurement(watermark-TimeUtil.YEAR_PERIOD_NS, 89F, 179F, 100001F, 100001F, false, true, watermark, Long.MAX_VALUE);
    }

    private Long timestamp;

    private Float latitude;
    private Float longitude;

    private Float p1; //Particles < 10µm (particulate matter)
    private Float p2; //Particles < 2.5µm (ultrafine particles)

    private Boolean current;

    private Boolean batchLast;

    private Long currentWatermark;

    private Long batchSeqId;

    private Long batchFirstTimestamp;

    public InputMeasurement(){

    }

    public InputMeasurement(Long timestamp, Float latitude, Float longitude, Float p1, Float p2, Boolean current, Boolean batchLast, Long currentWatermark, Long batchSeqId) {
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.p1 = p1;
        this.p2 = p2;
        this.current = current;
        this.batchLast = batchLast;
        this.currentWatermark = currentWatermark;
        this.batchSeqId = batchSeqId;
    }

    public InputMeasurement(Long timestamp, Float latitude, Float longitude, Float p1, Float p2, Boolean current, Boolean batchLast, Long currentWatermark, Long batchSeqId, Long batchFirstTimestamp) {
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.p1 = p1;
        this.p2 = p2;
        this.current = current;
        this.batchLast = batchLast;
        this.currentWatermark = currentWatermark;
        this.batchSeqId = batchSeqId;
        this.batchFirstTimestamp = batchFirstTimestamp;
    }

    public Long getBatchFirstTimestamp() {
        return batchFirstTimestamp;
    }

    public void setBatchFirstTimestamp(Long batchFirstTimestamp) {
        this.batchFirstTimestamp = batchFirstTimestamp;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getP1() {
        return p1;
    }

    public void setP1(Float p1) {
        this.p1 = p1;
    }

    public Float getP2() {
        return p2;
    }

    public void setP2(Float p2) {
        this.p2 = p2;
    }

    public Boolean getCurrent() {
        return current;
    }

    public void setCurrent(Boolean current) {
        this.current = current;
    }

    public Boolean getBatchLast() {
        return batchLast;
    }

    public void setBatchLast(Boolean batchLast) {
        this.batchLast = batchLast;
    }

    public Long getCurrentWatermark() {
        return currentWatermark;
    }

    public void setCurrentWatermark(Long currentWatermark) {
        this.currentWatermark = currentWatermark;
    }

    public Long getBatchSeqId() {
        return batchSeqId;
    }

    public void setBatchSeqId(Long batchSeqId) {
        this.batchSeqId = batchSeqId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InputMeasurement that = (InputMeasurement) o;
        return Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude) &&
                Objects.equals(p1, that.p1) &&
                Objects.equals(p2, that.p2) &&
                Objects.equals(current, that.current) &&
                Objects.equals(batchLast, that.batchLast) &&
                Objects.equals(currentWatermark, that.currentWatermark) &&
                Objects.equals(batchSeqId, that.batchSeqId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, latitude, longitude, p1, p2, current, batchLast, currentWatermark, batchSeqId);
    }

    @Override
    public String toString() {
        return "InputMeasurement{" +
                "timestamp=" + timestamp +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", p1=" + p1 +
                ", p2=" + p2 +
                ", current=" + current +
                ", batchLast=" + batchLast +
                ", currentWatermark=" + currentWatermark +
                ", batchSeqId=" + batchSeqId +
                '}';
    }

    @Override
    public int compareTo(InputMeasurement o) {
        return 0;
    }
}
