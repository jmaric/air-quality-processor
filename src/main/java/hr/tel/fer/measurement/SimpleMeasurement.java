package hr.tel.fer.measurement;

import java.util.Objects;

public class SimpleMeasurement {
    private long timestamp;

    private float p1; //Particles < 10µm (particulate matter)
    private float p2; //Particles < 2.5µm (ultrafine particles)

    public SimpleMeasurement() {

    }

    public SimpleMeasurement(long timestamp, float p1, float p2) {
        this.timestamp = timestamp;
        this.p1 = p1;
        this.p2 = p2;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public float getP1() {
        return p1;
    }

    public void setP1(float p1) {
        this.p1 = p1;
    }

    public float getP2() {
        return p2;
    }

    public void setP2(float p2) {
        this.p2 = p2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleMeasurement that = (SimpleMeasurement) o;
        return timestamp == that.timestamp &&
                Float.compare(that.p1, p1) == 0 &&
                Float.compare(that.p2, p2) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, p1, p2);
    }

    @Override
    public String toString() {
        return
//                timestamp + ";" +
                p1 +
                ";" + p2;
    }
}
