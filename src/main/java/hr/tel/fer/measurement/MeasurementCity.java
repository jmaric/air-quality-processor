package hr.tel.fer.measurement;

import java.util.Objects;

public class MeasurementCity {
    private SimpleMeasurement measurement;
    private String city;

    public MeasurementCity(){

    }

    public MeasurementCity(SimpleMeasurement measurement, String city) {
        this.measurement = measurement;
        this.city = city;
    }

    public SimpleMeasurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(SimpleMeasurement measurement) {
        this.measurement = measurement;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MeasurementCity that = (MeasurementCity) o;
        return Objects.equals(measurement, that.measurement) &&
                Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(measurement, city);
    }

    @Override
    public String toString() {
        return "MeasurementCity{" +
                "measurement=" + measurement +
                ", city='" + city + '\'' +
                '}';
    }
}
