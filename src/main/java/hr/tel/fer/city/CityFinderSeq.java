package hr.tel.fer.city;

import de.tum.i13.proto.Location;
import hr.tel.fer.polygon.PolygonCity;
import hr.tel.fer.polygon.PolygonTransformer;
import hr.tel.fer.polygon.SimplePoint;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class CityFinderSeq implements Serializable, CityFinder {
    private GeometryFactory geometryFactory;
    private PolygonTransformer polygonTransformer;
    private List<PolygonCity> polygonCities;
    private Map<SimplePoint, String> locatedPoints;
    private Envelope envelope;
    public CityFinderSeq(List<Location> locations){
        this.geometryFactory = new GeometryFactory();
        this.polygonTransformer = new PolygonTransformer();
        this.polygonCities = transformLocationsToPolygonCities(locations);
        this.locatedPoints = new HashMap<>();
        this.envelope = initializeEnvelope(polygonCities);
    }

    public String getCityFromPoint(float lon, float lat){
        Coordinate coord = new Coordinate(lon, lat);
        Point pt = geometryFactory.createPoint(coord);
//        if(!envelope.contains(coord)) return null;
        String city = locatedPoints.get(new SimplePoint(lon, lat));
        if (city != null) return city;
        for (PolygonCity polygonCity : polygonCities) {
            if(polygonCity.getPolgyon().contains(pt)){
                locatedPoints.put(new SimplePoint(lon, lat), polygonCity.getCity());
                return polygonCity.getCity();
            }
        }
        return null;
    }

    public boolean areCoordsInsideMinimumRoundingBox(float lon, float lat){
        Coordinate coord = new Coordinate(lon, lat);
        Point pt = geometryFactory.createPoint(coord);
        return envelope.contains(coord);
    }

    public boolean isPointLocatedAlready(float lon, float lat){
        return locatedPoints.containsKey(SimplePoint.of(lon, lat));
    }

    private List<PolygonCity> transformLocationsToPolygonCities(List<Location> locations){
        List<PolygonCity> polygonCities = new ArrayList<>();
        for (Location location : locations) {
            List<de.tum.i13.proto.Polygon> polygons = location.getPolygonsList();
            for(de.tum.i13.proto.Polygon polygon : polygons){
                polygonCities.add(new PolygonCity(polygonTransformer.transformPolyFromProtoToJts(polygon), location.getCity()));
            }
        }
//        System.out.println("List size: " + polygonCities.size());
        return polygonCities;
    }

    private List<Location> reverseSortLocationsByPopulation(List<Location> locations){
        return locations.stream()
                .sorted(Comparator.comparingInt(Location::getPopulation).reversed())
                .collect(Collectors.toList());
    }

    private Envelope initializeEnvelope(List<PolygonCity> polygonCities){
        Envelope env = new Envelope();
        for(PolygonCity pc : polygonCities){
            env.expandToInclude(pc.getPolgyon().getEnvelopeInternal());
        }
        return env;
    }

    public Envelope getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Envelope envelope) {
        this.envelope = envelope;
    }
}
