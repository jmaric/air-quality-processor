package hr.tel.fer.city;

import de.tum.i13.proto.Location;
import hr.tel.fer.polygon.PolygonCity;
import hr.tel.fer.polygon.PolygonTransformer;
import hr.tel.fer.polygon.SimplePoint;
import hr.tel.fer.spatial_index.SpatialIndexFactoryClean;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.index.SpatialIndex;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class CityFinderIndex implements Serializable, CityFinder {
    private GeometryFactory geometryFactory;
    private PolygonTransformer polygonTransformer;
    private SpatialIndex polygonCitiesIndex;
    private Map<SimplePoint, String> locatedPoints;
    private Envelope envelope;
    public CityFinderIndex(List<Location> locations){
        this.geometryFactory = new GeometryFactory();
        this.polygonTransformer = new PolygonTransformer();
        List<PolygonCity> polygonCities = transformLocationsToPolygonCities(locations);
        this.polygonCitiesIndex = SpatialIndexFactoryClean.create(SpatialIndexFactoryClean.IndexType.STR_TREE, polygonCities);
        this.locatedPoints = new HashMap<>();
        this.envelope = initializeEnvelope(polygonCities);
    }

    public String getCityFromPoint(float lon, float lat){
        String city = locatedPoints.get(new SimplePoint(lon, lat));
        if (city != null) return city;
        Coordinate coord = new Coordinate(lon, lat);
        Point pt = geometryFactory.createPoint(coord);
//        System.out.println("Point env: " + pt.getEnvelopeInternal());
        List<PolygonCity> queried = (List<PolygonCity>) polygonCitiesIndex.query(pt.getEnvelopeInternal());
//        System.out.println("Queried: " + queried);
        if(queried.size() == 0) return null;
//        else city = (String) queried.get(0);
//        if(queried.size() == 0) System.out.println("Zero.");
//        else if(queried.size() > 1) System.out.println("Many of them: " + queried);
//        else System.out.println("One.");
//        System.out.println("Queried size: " + queried.size());
        for (PolygonCity polygonCity : queried) {
            if(polygonCity.getPolgyon().contains(pt)){
                locatedPoints.put(new SimplePoint(lon, lat), polygonCity.getCity());
                return polygonCity.getCity();
            }
        }
        return city;
    }

    public boolean areCoordsInsideMinimumRoundingBox(float lon, float lat){
        Coordinate coord = new Coordinate(lon, lat);
        Point pt = geometryFactory.createPoint(coord);
        return envelope.contains(coord);
    }

    public boolean isPointLocatedAlready(float lon, float lat){
        return locatedPoints.containsKey(SimplePoint.of(lon, lat));
    }

    private List<PolygonCity> transformLocationsToPolygonCities(List<Location> locations){
        List<PolygonCity> polygonCities = new ArrayList<>();
        for (Location location : locations) {
            List<de.tum.i13.proto.Polygon> polygons = location.getPolygonsList();
            for(de.tum.i13.proto.Polygon polygon : polygons){
                polygonCities.add(new PolygonCity(polygonTransformer.transformPolyFromProtoToJts(polygon), location.getCity()));
            }
        }
//        System.out.println("List size: " + polygonCities.size());
        return polygonCities;
    }

    private List<Location> reverseSortLocationsByPopulation(List<Location> locations){
        return locations.stream()
                .sorted(Comparator.comparingInt(Location::getPopulation).reversed())
                .collect(Collectors.toList());
    }

    private Envelope initializeEnvelope(List<PolygonCity> polygonCities){
        Envelope env = new Envelope();
        for(PolygonCity pc : polygonCities){
            env.expandToInclude(pc.getPolgyon().getEnvelopeInternal());
        }
        return env;
    }

    public Envelope getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Envelope envelope) {
        this.envelope = envelope;
    }
}
