package hr.tel.fer.city;

import org.locationtech.jts.geom.Envelope;

public interface CityFinder {
    public String getCityFromPoint(float lon, float lat);

    public Envelope getEnvelope();
}
