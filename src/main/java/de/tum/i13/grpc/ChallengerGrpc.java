package de.tum.i13.grpc;

import de.tum.i13.proto.*;

import java.io.Serializable;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.33.1)",
    comments = "Source: challenger.proto")
public final class ChallengerGrpc {

  private ChallengerGrpc() {}

  public static final String SERVICE_NAME = "Challenger.Challenger";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<BenchmarkConfiguration,
          Benchmark> getCreateNewBenchmarkMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "createNewBenchmark",
      requestType = BenchmarkConfiguration.class,
      responseType = Benchmark.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<BenchmarkConfiguration,
      Benchmark> getCreateNewBenchmarkMethod() {
    io.grpc.MethodDescriptor<BenchmarkConfiguration, Benchmark> getCreateNewBenchmarkMethod;
    if ((getCreateNewBenchmarkMethod = ChallengerGrpc.getCreateNewBenchmarkMethod) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getCreateNewBenchmarkMethod = ChallengerGrpc.getCreateNewBenchmarkMethod) == null) {
          ChallengerGrpc.getCreateNewBenchmarkMethod = getCreateNewBenchmarkMethod =
              io.grpc.MethodDescriptor.<BenchmarkConfiguration, Benchmark>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "createNewBenchmark"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  BenchmarkConfiguration.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Benchmark.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("createNewBenchmark"))
              .build();
        }
      }
    }
    return getCreateNewBenchmarkMethod;
  }

  private static volatile io.grpc.MethodDescriptor<Benchmark,
          Locations> getGetLocationsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getLocations",
      requestType = Benchmark.class,
      responseType = Locations.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<Benchmark,
          Locations> getGetLocationsMethod() {
    io.grpc.MethodDescriptor<Benchmark, Locations> getGetLocationsMethod;
    if ((getGetLocationsMethod = ChallengerGrpc.getGetLocationsMethod) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getGetLocationsMethod = ChallengerGrpc.getGetLocationsMethod) == null) {
          ChallengerGrpc.getGetLocationsMethod = getGetLocationsMethod =
              io.grpc.MethodDescriptor.<Benchmark, Locations>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getLocations"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Benchmark.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Locations.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("getLocations"))
              .build();
        }
      }
    }
    return getGetLocationsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<Benchmark,
          Ping> getInitializeLatencyMeasuringMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "initializeLatencyMeasuring",
      requestType = Benchmark.class,
      responseType = Ping.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<Benchmark,
      Ping> getInitializeLatencyMeasuringMethod() {
    io.grpc.MethodDescriptor<Benchmark, Ping> getInitializeLatencyMeasuringMethod;
    if ((getInitializeLatencyMeasuringMethod = ChallengerGrpc.getInitializeLatencyMeasuringMethod) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getInitializeLatencyMeasuringMethod = ChallengerGrpc.getInitializeLatencyMeasuringMethod) == null) {
          ChallengerGrpc.getInitializeLatencyMeasuringMethod = getInitializeLatencyMeasuringMethod =
              io.grpc.MethodDescriptor.<Benchmark, Ping>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "initializeLatencyMeasuring"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Benchmark.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Ping.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("initializeLatencyMeasuring"))
              .build();
        }
      }
    }
    return getInitializeLatencyMeasuringMethod;
  }

  private static volatile io.grpc.MethodDescriptor<Ping,
      Ping> getMeasureMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "measure",
      requestType = Ping.class,
      responseType = Ping.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<Ping,
      Ping> getMeasureMethod() {
    io.grpc.MethodDescriptor<Ping, Ping> getMeasureMethod;
    if ((getMeasureMethod = ChallengerGrpc.getMeasureMethod) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getMeasureMethod = ChallengerGrpc.getMeasureMethod) == null) {
          ChallengerGrpc.getMeasureMethod = getMeasureMethod =
              io.grpc.MethodDescriptor.<Ping, Ping>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "measure"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Ping.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Ping.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("measure"))
              .build();
        }
      }
    }
    return getMeasureMethod;
  }

  private static volatile io.grpc.MethodDescriptor<Ping,
      com.google.protobuf.Empty> getEndMeasurementMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "endMeasurement",
      requestType = Ping.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<Ping,
      com.google.protobuf.Empty> getEndMeasurementMethod() {
    io.grpc.MethodDescriptor<Ping, com.google.protobuf.Empty> getEndMeasurementMethod;
    if ((getEndMeasurementMethod = ChallengerGrpc.getEndMeasurementMethod) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getEndMeasurementMethod = ChallengerGrpc.getEndMeasurementMethod) == null) {
          ChallengerGrpc.getEndMeasurementMethod = getEndMeasurementMethod =
              io.grpc.MethodDescriptor.<Ping, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "endMeasurement"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Ping.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("endMeasurement"))
              .build();
        }
      }
    }
    return getEndMeasurementMethod;
  }

  private static volatile io.grpc.MethodDescriptor<Benchmark,
      com.google.protobuf.Empty> getStartBenchmarkMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "startBenchmark",
      requestType = Benchmark.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<Benchmark,
      com.google.protobuf.Empty> getStartBenchmarkMethod() {
    io.grpc.MethodDescriptor<Benchmark, com.google.protobuf.Empty> getStartBenchmarkMethod;
    if ((getStartBenchmarkMethod = ChallengerGrpc.getStartBenchmarkMethod) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getStartBenchmarkMethod = ChallengerGrpc.getStartBenchmarkMethod) == null) {
          ChallengerGrpc.getStartBenchmarkMethod = getStartBenchmarkMethod =
              io.grpc.MethodDescriptor.<Benchmark, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "startBenchmark"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Benchmark.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("startBenchmark"))
              .build();
        }
      }
    }
    return getStartBenchmarkMethod;
  }

  private static volatile io.grpc.MethodDescriptor<Benchmark,
          Batch> getNextBatchMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "nextBatch",
      requestType = Benchmark.class,
      responseType = Batch.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<Benchmark,
      Batch> getNextBatchMethod() {
    io.grpc.MethodDescriptor<Benchmark, Batch> getNextBatchMethod;
    if ((getNextBatchMethod = ChallengerGrpc.getNextBatchMethod) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getNextBatchMethod = ChallengerGrpc.getNextBatchMethod) == null) {
          ChallengerGrpc.getNextBatchMethod = getNextBatchMethod =
              io.grpc.MethodDescriptor.<Benchmark, Batch>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "nextBatch"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Benchmark.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Batch.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("nextBatch"))
              .build();
        }
      }
    }
    return getNextBatchMethod;
  }

  private static volatile io.grpc.MethodDescriptor<ResultQ1,
      com.google.protobuf.Empty> getResultQ1Method;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "resultQ1",
      requestType = ResultQ1.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ResultQ1,
      com.google.protobuf.Empty> getResultQ1Method() {
    io.grpc.MethodDescriptor<ResultQ1, com.google.protobuf.Empty> getResultQ1Method;
    if ((getResultQ1Method = ChallengerGrpc.getResultQ1Method) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getResultQ1Method = ChallengerGrpc.getResultQ1Method) == null) {
          ChallengerGrpc.getResultQ1Method = getResultQ1Method =
              io.grpc.MethodDescriptor.<ResultQ1, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "resultQ1"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ResultQ1.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("resultQ1"))
              .build();
        }
      }
    }
    return getResultQ1Method;
  }

  private static volatile io.grpc.MethodDescriptor<ResultQ2,
      com.google.protobuf.Empty> getResultQ2Method;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "resultQ2",
      requestType = ResultQ2.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ResultQ2,
      com.google.protobuf.Empty> getResultQ2Method() {
    io.grpc.MethodDescriptor<ResultQ2, com.google.protobuf.Empty> getResultQ2Method;
    if ((getResultQ2Method = ChallengerGrpc.getResultQ2Method) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getResultQ2Method = ChallengerGrpc.getResultQ2Method) == null) {
          ChallengerGrpc.getResultQ2Method = getResultQ2Method =
              io.grpc.MethodDescriptor.<ResultQ2, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "resultQ2"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ResultQ2.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("resultQ2"))
              .build();
        }
      }
    }
    return getResultQ2Method;
  }

  private static volatile io.grpc.MethodDescriptor<Benchmark,
      com.google.protobuf.Empty> getEndBenchmarkMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "endBenchmark",
      requestType = Benchmark.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<Benchmark,
      com.google.protobuf.Empty> getEndBenchmarkMethod() {
    io.grpc.MethodDescriptor<Benchmark, com.google.protobuf.Empty> getEndBenchmarkMethod;
    if ((getEndBenchmarkMethod = ChallengerGrpc.getEndBenchmarkMethod) == null) {
      synchronized (ChallengerGrpc.class) {
        if ((getEndBenchmarkMethod = ChallengerGrpc.getEndBenchmarkMethod) == null) {
          ChallengerGrpc.getEndBenchmarkMethod = getEndBenchmarkMethod =
              io.grpc.MethodDescriptor.<Benchmark, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "endBenchmark"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  Benchmark.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ChallengerMethodDescriptorSupplier("endBenchmark"))
              .build();
        }
      }
    }
    return getEndBenchmarkMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ChallengerStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ChallengerStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ChallengerStub>() {
        @Override
        public ChallengerStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ChallengerStub(channel, callOptions);
        }
      };
    return ChallengerStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ChallengerBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ChallengerBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ChallengerBlockingStub>() {
        @Override
        public ChallengerBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ChallengerBlockingStub(channel, callOptions);
        }
      };
    return ChallengerBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ChallengerFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ChallengerFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ChallengerFutureStub>() {
        @Override
        public ChallengerFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ChallengerFutureStub(channel, callOptions);
        }
      };
    return ChallengerFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class ChallengerImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     *Create a new Benchmark based on the configuration
     * </pre>
     */
    public void createNewBenchmark(BenchmarkConfiguration request,
                                   io.grpc.stub.StreamObserver<Benchmark> responseObserver) {
      asyncUnimplementedUnaryCall(getCreateNewBenchmarkMethod(), responseObserver);
    }

    /**
     * <pre>
     *Get the polygons of all zip areas in germany based on the benchmarktype
     * </pre>
     */
    public void getLocations(Benchmark request,
                             io.grpc.stub.StreamObserver<Locations> responseObserver) {
      asyncUnimplementedUnaryCall(getGetLocationsMethod(), responseObserver);
    }

    /**
     * <pre>
     * Depending on your connectivity you have a latency and throughput.
     * Optionally, we try to account for this by first measuring it.
     * The payload of a Ping corresponds roughly to the payload of a batch and the returning Pong roughly the payload of a Result
     * This kind of measurement is just for development and experimentation (since it could be easily cheated ;-))
     * We do not consider that once you deploy your implementation on the VMs in our infrastructure
     * </pre>
     */
    public void initializeLatencyMeasuring(Benchmark request,
                                           io.grpc.stub.StreamObserver<Ping> responseObserver) {
      asyncUnimplementedUnaryCall(getInitializeLatencyMeasuringMethod(), responseObserver);
    }

    /**
     */
    public void measure(Ping request,
                        io.grpc.stub.StreamObserver<Ping> responseObserver) {
      asyncUnimplementedUnaryCall(getMeasureMethod(), responseObserver);
    }

    /**
     */
    public void endMeasurement(Ping request,
                               io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getEndMeasurementMethod(), responseObserver);
    }

    /**
     * <pre>
     *This marks the starting point of the throughput measurements
     * </pre>
     */
    public void startBenchmark(Benchmark request,
                               io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getStartBenchmarkMethod(), responseObserver);
    }

    /**
     * <pre>
     *get the next Batch
     * </pre>
     */
    public void nextBatch(Benchmark request,
                          io.grpc.stub.StreamObserver<Batch> responseObserver) {
      asyncUnimplementedUnaryCall(getNextBatchMethod(), responseObserver);
    }

    /**
     * <pre>
     *post the result
     * </pre>
     */
    public void resultQ1(ResultQ1 request,
                         io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getResultQ1Method(), responseObserver);
    }

    /**
     */
    public void resultQ2(ResultQ2 request,
                         io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getResultQ2Method(), responseObserver);
    }

    /**
     * <pre>
     *This marks the end of the throughput measurements
     * </pre>
     */
    public void endBenchmark(Benchmark request,
                             io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getEndBenchmarkMethod(), responseObserver);
    }

    @Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getCreateNewBenchmarkMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                BenchmarkConfiguration,
                Benchmark>(
                  this, METHODID_CREATE_NEW_BENCHMARK)))
          .addMethod(
            getGetLocationsMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                Benchmark,
                Locations>(
                  this, METHODID_GET_LOCATIONS)))
          .addMethod(
            getInitializeLatencyMeasuringMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                Benchmark,
                Ping>(
                  this, METHODID_INITIALIZE_LATENCY_MEASURING)))
          .addMethod(
            getMeasureMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                Ping,
                Ping>(
                  this, METHODID_MEASURE)))
          .addMethod(
            getEndMeasurementMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                Ping,
                com.google.protobuf.Empty>(
                  this, METHODID_END_MEASUREMENT)))
          .addMethod(
            getStartBenchmarkMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                Benchmark,
                com.google.protobuf.Empty>(
                  this, METHODID_START_BENCHMARK)))
          .addMethod(
            getNextBatchMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                Benchmark,
                Batch>(
                  this, METHODID_NEXT_BATCH)))
          .addMethod(
            getResultQ1Method(),
            asyncUnaryCall(
              new MethodHandlers<
                ResultQ1,
                com.google.protobuf.Empty>(
                  this, METHODID_RESULT_Q1)))
          .addMethod(
            getResultQ2Method(),
            asyncUnaryCall(
              new MethodHandlers<
                ResultQ2,
                com.google.protobuf.Empty>(
                  this, METHODID_RESULT_Q2)))
          .addMethod(
            getEndBenchmarkMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                Benchmark,
                com.google.protobuf.Empty>(
                  this, METHODID_END_BENCHMARK)))
          .build();
    }
  }

  /**
   */
  public static final class ChallengerStub extends io.grpc.stub.AbstractAsyncStub<ChallengerStub> {
    private ChallengerStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected ChallengerStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ChallengerStub(channel, callOptions);
    }

    /**
     * <pre>
     *Create a new Benchmark based on the configuration
     * </pre>
     */
    public void createNewBenchmark(BenchmarkConfiguration request,
                                   io.grpc.stub.StreamObserver<Benchmark> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCreateNewBenchmarkMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     *Get the polygons of all zip areas in germany based on the benchmarktype
     * </pre>
     */
    public void getLocations(Benchmark request,
                             io.grpc.stub.StreamObserver<Locations> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetLocationsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * Depending on your connectivity you have a latency and throughput.
     * Optionally, we try to account for this by first measuring it.
     * The payload of a Ping corresponds roughly to the payload of a batch and the returning Pong roughly the payload of a Result
     * This kind of measurement is just for development and experimentation (since it could be easily cheated ;-))
     * We do not consider that once you deploy your implementation on the VMs in our infrastructure
     * </pre>
     */
    public void initializeLatencyMeasuring(Benchmark request,
                                           io.grpc.stub.StreamObserver<Ping> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getInitializeLatencyMeasuringMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void measure(Ping request,
                        io.grpc.stub.StreamObserver<Ping> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getMeasureMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void endMeasurement(Ping request,
                               io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getEndMeasurementMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     *This marks the starting point of the throughput measurements
     * </pre>
     */
    public void startBenchmark(Benchmark request,
                               io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getStartBenchmarkMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     *get the next Batch
     * </pre>
     */
    public void nextBatch(Benchmark request,
                          io.grpc.stub.StreamObserver<Batch> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getNextBatchMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     *post the result
     * </pre>
     */
    public void resultQ1(ResultQ1 request,
                         io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getResultQ1Method(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void resultQ2(ResultQ2 request,
                         io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getResultQ2Method(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     *This marks the end of the throughput measurements
     * </pre>
     */
    public void endBenchmark(Benchmark request,
                             io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getEndBenchmarkMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ChallengerBlockingStub extends io.grpc.stub.AbstractBlockingStub<ChallengerBlockingStub> implements Serializable {
    public ChallengerBlockingStub(
            io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected ChallengerBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ChallengerBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     *Create a new Benchmark based on the configuration
     * </pre>
     */
    public Benchmark createNewBenchmark(BenchmarkConfiguration request) {
      return blockingUnaryCall(
          getChannel(), getCreateNewBenchmarkMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     *Get the polygons of all zip areas in germany based on the benchmarktype
     * </pre>
     */
    public Locations getLocations(Benchmark request) {
      return blockingUnaryCall(
          getChannel(), getGetLocationsMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * Depending on your connectivity you have a latency and throughput.
     * Optionally, we try to account for this by first measuring it.
     * The payload of a Ping corresponds roughly to the payload of a batch and the returning Pong roughly the payload of a Result
     * This kind of measurement is just for development and experimentation (since it could be easily cheated ;-))
     * We do not consider that once you deploy your implementation on the VMs in our infrastructure
     * </pre>
     */
    public Ping initializeLatencyMeasuring(Benchmark request) {
      return blockingUnaryCall(
          getChannel(), getInitializeLatencyMeasuringMethod(), getCallOptions(), request);
    }

    /**
     */
    public Ping measure(Ping request) {
      return blockingUnaryCall(
          getChannel(), getMeasureMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty endMeasurement(Ping request) {
      return blockingUnaryCall(
          getChannel(), getEndMeasurementMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     *This marks the starting point of the throughput measurements
     * </pre>
     */
    public com.google.protobuf.Empty startBenchmark(Benchmark request) {
      return blockingUnaryCall(
          getChannel(), getStartBenchmarkMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     *get the next Batch
     * </pre>
     */
    public Batch nextBatch(Benchmark request) {
      return blockingUnaryCall(
          getChannel(), getNextBatchMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     *post the result
     * </pre>
     */
    public com.google.protobuf.Empty resultQ1(ResultQ1 request) {
      return blockingUnaryCall(
          getChannel(), getResultQ1Method(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty resultQ2(ResultQ2 request) {
      return blockingUnaryCall(
          getChannel(), getResultQ2Method(), getCallOptions(), request);
    }

    /**
     * <pre>
     *This marks the end of the throughput measurements
     * </pre>
     */
    public com.google.protobuf.Empty endBenchmark(Benchmark request) {
      return blockingUnaryCall(
          getChannel(), getEndBenchmarkMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ChallengerFutureStub extends io.grpc.stub.AbstractFutureStub<ChallengerFutureStub> {
    private ChallengerFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected ChallengerFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ChallengerFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     *Create a new Benchmark based on the configuration
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<Benchmark> createNewBenchmark(
        BenchmarkConfiguration request) {
      return futureUnaryCall(
          getChannel().newCall(getCreateNewBenchmarkMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     *Get the polygons of all zip areas in germany based on the benchmarktype
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<Locations> getLocations(
        Benchmark request) {
      return futureUnaryCall(
          getChannel().newCall(getGetLocationsMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * Depending on your connectivity you have a latency and throughput.
     * Optionally, we try to account for this by first measuring it.
     * The payload of a Ping corresponds roughly to the payload of a batch and the returning Pong roughly the payload of a Result
     * This kind of measurement is just for development and experimentation (since it could be easily cheated ;-))
     * We do not consider that once you deploy your implementation on the VMs in our infrastructure
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<Ping> initializeLatencyMeasuring(
        Benchmark request) {
      return futureUnaryCall(
          getChannel().newCall(getInitializeLatencyMeasuringMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<Ping> measure(
        Ping request) {
      return futureUnaryCall(
          getChannel().newCall(getMeasureMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> endMeasurement(
        Ping request) {
      return futureUnaryCall(
          getChannel().newCall(getEndMeasurementMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     *This marks the starting point of the throughput measurements
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> startBenchmark(
        Benchmark request) {
      return futureUnaryCall(
          getChannel().newCall(getStartBenchmarkMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     *get the next Batch
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<Batch> nextBatch(
        Benchmark request) {
      return futureUnaryCall(
          getChannel().newCall(getNextBatchMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     *post the result
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> resultQ1(
        ResultQ1 request) {
      return futureUnaryCall(
          getChannel().newCall(getResultQ1Method(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> resultQ2(
        ResultQ2 request) {
      return futureUnaryCall(
          getChannel().newCall(getResultQ2Method(), getCallOptions()), request);
    }

    /**
     * <pre>
     *This marks the end of the throughput measurements
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> endBenchmark(
        Benchmark request) {
      return futureUnaryCall(
          getChannel().newCall(getEndBenchmarkMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_CREATE_NEW_BENCHMARK = 0;
  private static final int METHODID_GET_LOCATIONS = 1;
  private static final int METHODID_INITIALIZE_LATENCY_MEASURING = 2;
  private static final int METHODID_MEASURE = 3;
  private static final int METHODID_END_MEASUREMENT = 4;
  private static final int METHODID_START_BENCHMARK = 5;
  private static final int METHODID_NEXT_BATCH = 6;
  private static final int METHODID_RESULT_Q1 = 7;
  private static final int METHODID_RESULT_Q2 = 8;
  private static final int METHODID_END_BENCHMARK = 9;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ChallengerImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ChallengerImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CREATE_NEW_BENCHMARK:
          serviceImpl.createNewBenchmark((BenchmarkConfiguration) request,
              (io.grpc.stub.StreamObserver<Benchmark>) responseObserver);
          break;
        case METHODID_GET_LOCATIONS:
          serviceImpl.getLocations((Benchmark) request,
              (io.grpc.stub.StreamObserver<Locations>) responseObserver);
          break;
        case METHODID_INITIALIZE_LATENCY_MEASURING:
          serviceImpl.initializeLatencyMeasuring((Benchmark) request,
              (io.grpc.stub.StreamObserver<Ping>) responseObserver);
          break;
        case METHODID_MEASURE:
          serviceImpl.measure((Ping) request,
              (io.grpc.stub.StreamObserver<Ping>) responseObserver);
          break;
        case METHODID_END_MEASUREMENT:
          serviceImpl.endMeasurement((Ping) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_START_BENCHMARK:
          serviceImpl.startBenchmark((Benchmark) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_NEXT_BATCH:
          serviceImpl.nextBatch((Benchmark) request,
              (io.grpc.stub.StreamObserver<Batch>) responseObserver);
          break;
        case METHODID_RESULT_Q1:
          serviceImpl.resultQ1((ResultQ1) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_RESULT_Q2:
          serviceImpl.resultQ2((ResultQ2) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_END_BENCHMARK:
          serviceImpl.endBenchmark((Benchmark) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @Override
    @SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ChallengerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ChallengerBaseDescriptorSupplier() {}

    @Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ChallengerProto.getDescriptor();
    }

    @Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Challenger");
    }
  }

  private static final class ChallengerFileDescriptorSupplier
      extends ChallengerBaseDescriptorSupplier {
    ChallengerFileDescriptorSupplier() {}
  }

  private static final class ChallengerMethodDescriptorSupplier
      extends ChallengerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ChallengerMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ChallengerGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ChallengerFileDescriptorSupplier())
              .addMethod(getCreateNewBenchmarkMethod())
              .addMethod(getGetLocationsMethod())
              .addMethod(getInitializeLatencyMeasuringMethod())
              .addMethod(getMeasureMethod())
              .addMethod(getEndMeasurementMethod())
              .addMethod(getStartBenchmarkMethod())
              .addMethod(getNextBatchMethod())
              .addMethod(getResultQ1Method())
              .addMethod(getResultQ2Method())
              .addMethod(getEndBenchmarkMethod())
              .build();
        }
      }
    }
    return result;
  }
}
